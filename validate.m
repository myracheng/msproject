% new - map generated; gt - ground truth; pts - # of different thresholds
% used
function validate(new, gt, pts)
%% Validation:sensitivity, specificity, accuracy, positive predictive value, and dice similarity index
%sensitivity: amount deteceted. specificity: true negative/all negatives
%accuracies: al true/all ppv = true postiive/all postive dsi = 2 * true
%positives / each of segmented sets. rock curve, PR curve
% use parameters seg + gt(mask) ? or just gt.
% make sure both are boolean matrices
se = @(auto_seg,manual_seg) nnz(auto_seg & manual_seg)/nnz(manual_seg); %here tp/fn + tp = recall
sp = @(auto_seg,manual_seg) nnz(~auto_seg & ~manual_seg)/nnz(~manual_seg); %denominator is all negatives detected
a = @(auto_seg,manual_seg) nnz(or(~auto_seg & ~manual_seg, auto_seg & manual_seg))/numel(manual_seg);
ppv = @(auto_seg,manual_seg) nnz(auto_seg & manual_seg)/nnz(auto_seg); %here tp/fp + tp = precision
dice = @(auto_seg,manual_seg) 2*nnz(auto_seg & manual_seg)/(nnz(auto_seg) + nnz(manual_seg));
%% Generate table of data

%only care about masked part...try more though just to graph it.
sem = zeros(pts, 1);
spm = zeros(pts, 1);
am = zeros(pts, 1);
ppvm = zeros(pts, 1);
dicem = zeros(pts, 1);

% sem, spm, am, ppvm, dicem = deal(zeros(10,1));
for N = 1:pts
    thr = N*30;
    seg = new>thr;
    sem(N) = se(seg, gt>0);
    spm(N) = sp(seg, gt>0);
    am(N) = a(seg, gt>0);
    ppvm(N) = ppv(seg, gt>0);
    dicem(N) = dice(seg, gt>0);
    %     m for matrix
    %     spm =
    %     am =
    %     ppvm =
    %     dicem =
    %     val = strcat('se', num2str(thr));
    %     vals.(val) = se(seg, gt>0);
    %     val = strcat('sp', num2str(thr));
    %     vals.(val) = sp(seg, gt>0);
    %     val = strcat('a', num2str(thr));
    %     vals.(val) = a(seg, gt>0);
    %     val = strcat('ppv', num2str(thr));
    %     vals.(val) = ppv(seg, gt>0);
    %     val = strcat('dice', num2str(thr));
    %     vals.(val) = dice(seg, gt>0);
end
% sem
%
% dicem
%
% ppvm

% vals
%% Plots of validation measurements
figure;
plot(sem, '-or');
hold on;
plot(spm, '-om');
hold on;
plot(am, '-oc');
hold on;
plot(ppvm, '-og');
hold on;
plot(dicem, '-ob');
hold on;
% plot
%%
% homemade roc curve for validation only:
figure
a  = 'hi'
xfun = @(thr) 1-(nnz(~(gt > 0) & new<thr)/nnz(~(gt > 0))); %1-sp
yfun = @(thr) nnz(gt & new>=thr)/nnz(gt); %se
x = []; y=[];
for thr = [0 40 70 100 130]%[2,3,4,7]%unique(TIRADS)'%
    x = [x;xfun(thr)];
    y = [y;yfun(thr)];
end
x = [x;0];y = [y;0];
%[~,ix] = sort(y);
clf, plot(x(:),y(:),'.-')
axis square
AUC = 0.5*sum( (x(2:end)-x(1:end-1)).*(y(2:end)+y(1:end-1)) )
% AUC = abs(AUC);

%% Plots of curves - PR and AUC
% precision = ppv, recall = sv
% 1 - sp = fpr = fp / n
figure;
plot(sem, ppvm);
title('PR Curve');
xlabel('Recall');
ylabel('Precision');
figure;
plot(1-spm, sem, '-or');
% 1-spm
title('ROC Curve');
xlabel('False Positive Rate (1-specificity)');
ylabel('Sensitivity');
spm = [spm; 0; 1];
sem = [sem; 1; 0];
auc = trapz(1-spm, sem)
%% perfcurve function
% AUCval = zeros(size(spm))
% for N = 1:size(spm)
%     [X,Y,T,AUC] = perfcurve(1-spm, sem, 1-spm(N));
%     AUC
%     AUCval(N) = AUC;
% end
% AUCval

 [X,Y,T,AUC] = perfcurve(gt(mask) > 0, new(mask), 1)


%% calculate AUC
manualauc = 0;
for N = size(sem):-1:2
    a = 1-spm(N);
    b = 1-spm(N-1);
    a
    b
    term = (sem(N) + sem(N-1))*(a-b)/2;
    manualauc = manualauc + term;
end
manualauc
end