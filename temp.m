
% all stats tests
%% init vars + names
colors = [1,0,0;1,0,0;1,0,0;0,0,1;0,0,1;0,0,1]

imgs = {'method5'};
% imgs = {'texturedflair'};

% imgs = {'baselinet', 'neighbaseline', 'texturedflair', 'baselineneigbdfltext'};
% 15 files for each, generate 5 values for each file. 5*15*4 = 300
% patients = [15, 16]
patients = [1,2,4,5,6,7,8,10,12,14,15,16,17,18,19]; %no AUC = 0 no disease progression from combine.m
% patients = [1];
% rocX = [];
% rocY = [];
% prX = {};
% prY = {};
AUC = [];
PR = [];
PPV = [];
DSI = [];
SE = [];
OPT = [];
%% calculate values for each
for i = 1:length(imgs)
    auc = [];
    rocx = [];
    rocy = [];
    prx = [];
    pry = [];
    pr = [];
    ppv = [];
    dsi = [];
    se = [];
    opt = [];
    name = imgs{i};
    for J = patients %J is patient number
        %% load actual predictions
        num = num2str(J);
        folder = strcat('../genMSdata2/patient', num);
        filePattern = fullfile(folder, strcat('*', name,'*')); % Change to whatever pattern you need.
        theFiles = dir(filePattern);
        s.patient_number = num;
        fullFileName = [];
        for k = 1 : length(theFiles)
            baseFile = theFiles(k).name;
            if k == 1
                a = getfield(load_nii(fullfile(folder, baseFile)),'img');
            elseif k == 2
                b = getfield(load_nii(fullfile(folder, baseFile)),'img');
            elseif k == 3
                c = getfield(load_nii(fullfile(folder, baseFile)),'img');
            end
        end
        gt3 = getfield(load_nii(strcat('../MSpatientdata/patient',num,'/patient',num,'_gt3.nii')),'img')==1;
        if strfind(name, 'text')
            mask = zeros(size(gt3));
            directoryName = strcat('texturepatches3/patient', num);
            inds = h5read(strcat(directoryName, '/patient', num,'_unbalancedtextureProps.h5'),'/inds');
            mask(inds)= 1;
            mask = logical(mask);
      else
%             fprintf('yay')
            mask = getfield(load_nii(strcat('../MSpatientdata/patient',num,'/patient',num,'_brainmask.nii.gz')),'img')>0;
      
        end
        gt = gt3(mask);
        a = a(mask);
        b = b(mask);
        c = c(mask);
    
    iter = {a, b, c};
        for i = 1:length(iter) 
           new = iter{i};
           new(isnan(new) |isinf(new))=0;
             %% AUC, OPT
            [X,Y,T,AUCROC,OPTROCPT] = perfcurve(gt, new, true,'xCrit','FPR','TVals',min(new):range(new)/100:max(new));%testGroundTruth{J}
%             AUCROC;
                auc = [auc; AUCROC];
%             thr = T(find(X>=OPTROCPT(1),1));
%             opt = [opt; thr];
            rocx = [rocx, X];
            rocy = [rocy, Y];

%             plot(X,Y);
%             axis square
%%         AUCPR         
            [X,Y,T,AUCPR,OPTROCPT] = perfcurve(gt, new, true,'xCrit','PPV','TVals',min(new):range(new)/100:max(new));%testGroundTruth{J}
            if AUCPR < 0.5
                X(find(X == 0)) = 1;
            end
            AUCPRreal = abs(trapz(X, Y));
            pr = [pr; AUCPRreal];
            prx = [prx, X];
            pry = [pry, Y];
%                 hold on;
%                 plot(X,Y);
%             %     title = strcat
%                 title('Test Data PR');
%                 axis square
%             %% dsi
%                 dice = @(auto_seg,manual_seg) 2*nnz(auto_seg & manual_seg)/(nnz(auto_seg) + nnz(manual_seg));
%                sens = @(auto_seg,manual_seg) nnz(auto_seg & manual_seg)/nnz(manual_seg); %here tp/fn + tp = recall
%                 ppval = @(auto_seg,manual_seg) nnz(auto_seg & manual_seg)/nnz(auto_seg); %here tp/fp + tp = precision
% 
%                 dsitest = new;
%                 dsitest(dsitest > 0.75) = 1;
%                 dsitest(dsitest < 0.75) = 0;
%                 dsi =[dsi; dice(dsitest, gt)];
%                 se =[se; sens(dsitest, gt)];
%                 ppv =[ppv; ppval(dsitest, gt)];


                
        end
%         figure;
% %         subplot(1,2,1);
%         plot(rocx, rocy);
%         subplot(1,2,2);
%         plot(prx, pry);
%                 title(strcat('PR Curve for Patient', num, 'FontName', 'TimesNewRoman', 'FontSize', '20')
   
    end
AUC = [AUC, auc];
PR = [PR, pr];
PPV = [PPV, ppv];
DSI = [DSI, dsi];
SE = [SE, se];
OPT = [OPT, opt];
% rocX = [rocX, rocx];
% rocY = [rocY, rocy];
% prX(end+1) = {prx};
% prY(end+1) = {pry};
end

% [p,h] = signrank(testAUCs_baseline,testAUCs)
%% gen sample auc's
% ADD TITLES, "FPR", LEGEND
subplot(1,2,1)
set(gca, 'ColorOrder',colors(1:3,:),'NextPlot', 'replacechildren');
h1 = plot(tempX(:,1:3), tempY(:,1:3),'LineWidth',3);
set(gca, 'ColorOrder',colors(4:6,:),'NextPlot', 'replacechildren');
hold on
h2 = plot(tempX(:,4:6), tempY(:,4:6),'LineWidth',3)
set(gca, 'ColorOrder',colors(4:6,:),'NextPlot', 'replacechildren');
xlabel('1 - Specificity','FontName', 'Times New Roman', 'FontSize', 15)
ylabel('Sensitivity','FontName', 'Times New Roman', 'FontSize', 15)
% PR
subplot(1,2,2)
set(gca, 'ColorOrder',colors(1:3,:),'NextPlot', 'replacechildren');
h1 = plot(tempx(:,1:3), tempy(:,1:3),'LineWidth',3);
set(gca, 'ColorOrder',colors(4:6,:),'NextPlot', 'replacechildren');
hold on
h2 = plot(tempx(:,4:6), tempy(:,4:6),'LineWidth',3)
set(gca, 'ColorOrder',colors(4:6,:),'NextPlot', 'replacechildren');
xlabel('Precision','FontName', 'Times New Roman', 'FontSize', 15)
ylabel('Recall','FontName', 'Times New Roman', 'FontSize', 15)
%% s
i = 11
plot(prX(:,(((i-1)*3)+1):(i*3)), prY(:,(((i-1)*3)+1):(i*3)))

