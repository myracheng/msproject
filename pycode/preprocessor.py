import os
import re
import SimpleITK as sitk
import numpy as np
from pylab import *

from shutil import copyfile

def _listdir(directory):
    """A specialized version of os.listdir() that ignores files that
    start with a leading period."""
    filelist = sorted(os.listdir(directory))
    return [x for x in filelist if not (x.startswith('.'))]

def loadBRATS(pth):
    # example of usage:
    # import SimpleITK as sitk
    # from pylab import *
    # %matplotlib inline
    #
    # pth = 'C:/alfiia/projects/SU/BrainTumor/BRATS2015_Training/original'
    # bratsDB = loadBRATS(pth)
    #
    # stype = 'HGG'; icase = 0; smod = 'T1c'
    # imageFile = '%s/%s'%(pthOrig,bratsDB[stype][icase][smod])
    # print imageFile
    #
    # inputImage = sitk.ReadImage(imageFile)
    # data = sitk.GetArrayFromImage(inputImage)
    #
    # _, ax = plt.subplots(1,3)
    # ax[0].imshow(data[80,:,:],interpolation="nearest")
    # ax[1].imshow(data[:,100,:],interpolation="nearest")
    # ax[2].imshow(data[:,:,100],interpolation="nearest")
    # collect the HGG (high-grade) and LGG (low-grade) glioma cases
    bratsDB = {}
    smods = {'T1.', 'T2.', 'Flair.', 'T1c.', 'OT.'}
    fmods = {'T1.': 'T1', 'T2.': 'T2', 'Flair.': 'FLAIR', 'T1c.': 'T1c', 'OT.': 'GT'}
    nullDBEntry = {'T1': '', 'T2': '', 'FLAIR': '', 'T1c': '', 'GT': ''}
    for stype in ['HGG', 'LGG']:
        # go through both tumor types
        curpth = '%s/%s' % (pth, stype)
        dirs = _listdir(curpth)
        bratsDB[stype] = []
        # through all the cases (ie subjects):
        for icase in range(len(dirs)):
            casedirs = _listdir('%s/%s' % (curpth, dirs[icase]))
            dbEntry = nullDBEntry
            # collect all the image modalities (including Ground Truth):
            for imod in range(len(casedirs)):
                tmpdirs = _listdir('%s/%s/%s' % (curpth, dirs[icase], casedirs[imod]))
                for ifiles in range(len(tmpdirs)):
                    for imods in smods:
                        if imods in tmpdirs[ifiles]:
                            str = '%s/%s/%s/%s' % (stype, dirs[icase], casedirs[imod], tmpdirs[ifiles])
                            dbEntry[fmods[imods]] = str
            bratsDB[stype].append(
                {'T1': dbEntry['T1'], 'T2': dbEntry['T2'], 'T1c': dbEntry['T1c'], 'FLAIR': dbEntry['FLAIR'],
                 'GT': dbEntry['GT']})
    return bratsDB # TODO: make GT's entry '' if no such file (ie test)


def biasFieldCorrector(imageFile, outputFile, numberOfFittingLevels=4, maxIteration=50,
                       convergenceThreshold=1e-6, maskFile=''):
    # example of usage:
    #     import preprocessor as prep
    # import SimpleITK as sitk
    # import os
    #
    # pthOrig = 'C:/alfiia/projects/SU/BrainTumor/BRATS13/Training'
    # pthBFC = pthOrig+'_BFC'
    # bratsDB = prep.loadBRATS(pthOrig)
    #
    # for stype in {'HGG','LGG'}:
    #     for icase in range(len(bratsDB[stype])):
    #         for smod in {'T1','T1c'}:
    #             imageFile = '%s/%s'%(pthOrig,bratsDB[stype][icase][smod])
    #             outputFile = '%s/%s'%(pthBFC,bratsDB[stype][icase][smod])
    #             outputDir = os.path.dirname(outputFile)
    #             print outputFile
    #             if not os.path.isdir(outputDir):
    #                 os.makedirs(outputDir)
    #             prep.biasFieldCorrector(imageFile,outputFile)
    #
    inputImage = sitk.ReadImage(imageFile)

    # nda = sitk.GetArrayFromImage(inputImage)
    # ind = np.where(nda > 0)
    # nda[ind] = 1
    # maskImage = sitk.GetImageFromArray(nda)
    # maskImage.CopyInformation(inputImage)

    #inputImage = sitk.Shrink(inputImage, [int(shrinkFactor)] * inputImage.GetDimension())
    # maskImage = sitk.Shrink(maskImage, [int(shrinkFactor)] * inputImage.GetDimension())

    inputImage = sitk.Cast(inputImage, sitk.sitkFloat32)
    # maskImage = sitk.Cast(maskImage, sitk.sitkUInt8)

    if maskFile=='':
        maskImage = sitk.BinaryThreshold(inputImage, 1, 4095)
    else:
        maskImage = sitk.ReadImage(maskFile)

    # corrector = sitk.N4BiasFieldCorrectionImageFilter()
    #
    # corrector.SetMaximumNumberOfIterations([int(maxIteration)] * numberOfFittingLevels)
    # corrector.SetConvergenceThreshold(convergenceThreshold)
    #
    # output = corrector.Execute(inputImage, maskImage)

    output = sitk.N4BiasFieldCorrection(inputImage, maskImage, convergenceThreshold=convergenceThreshold, maximumNumberOfIterations=[int(maxIteration)]*numberOfFittingLevels)
    # N4BiasFieldCorrection(Image image, Image maskImage, double convergenceThreshold=0.001, VectorUInt32 maximumNumberOfIterations, double biasFieldFullWidthAtHalfMaximum=0.15, double wienerFilterNoise=0.01, uint32_t numberOfHistogramBins=200, VectorUInt32 numberOfControlPoints, uint32_t splineOrder=3) -> Image
    sitk.WriteImage(output, outputFile)
    return


def _quantile(data, q):
    # print quantile(np.array([1,2,3,4,5,6,7,8,9,10]),0.75)
    data = data.flatten()
    data = sort(data)
    n = len(data)
    ind = min(n-1,int(round(q*n)))
    if ind-q*n==0:
        qvalue = data[ind-1]+(data[ind]-data[ind-1])*q
    else:
        qvalue = data[ind-1]
    return qvalue


def histogramMatchingTraining(listImages, p_low=0.01, p_high=0.99, i_min=1, i_max=4095, nquantiles=10):
    # example of usage:
    # import SimpleITK as sitk
    # from pylab import *
    # %matplotlib inline
    # import preprocessor as prep
    #
    # pthOrig = 'C:/alfiia/projects/SU/BrainTumor/BRATS13/Training'
    # bratsDB = prep.loadBRATS(pthOrig)
    #
    # listImages = {}
    # modNames = {'T1', 'T2', 'FLAIR', 'T1c'}
    # for smods in modNames:
    #     listImages[smods] = []
    #     for stype in ['HGG','LGG']:
    #         for icase in range(len(bratsDB[stype])):
    #             listImages[smods].append('%s/%s'%(pthOrig,bratsDB[stype][icase][smods]))
    #
    # trainedQ = dict.fromkeys(modNames,{'ref_deciles':[],'qs':[]})
    # for smods in modNames:
    #     ref_deciles, qs = prep.histogramMatchingTraining(listImages[smods],nquantiles=4)
    #     plot(ref_deciles)
    #     trainedQ[smods]['ref_deciles']=list(np.round(ref_deciles,decimals=0))
    #     trainedQ[smods]['qs']=list(np.round(qs,decimals=2))
    # print trainedQ
    # import json
    # jsonFile = '%s/%s'%(pthOrig, 'quantileValues.json')
    # with open(jsonFile, 'w') as fp:
    #     json.dump(trainedQ, fp)


    nCases = len(listImages)
    qs = np.zeros(nquantiles+1)
    qs[0] = p_low
    qs[1:nquantiles] = np.arange(1.0/nquantiles,p_high,1.0/nquantiles)
    qs[nquantiles] = p_high
    Q = np.zeros(nquantiles+1) #a storage for all image quantiles including borders

    for icase in range(1,nCases):
        imageFile = listImages[icase]
        inputImage = sitk.ReadImage(imageFile)
        data = np.float32(sitk.GetArrayFromImage(inputImage))

        # define the intensities of interest
        IOI = data>min(data.flatten())

        # intensities of the j-th image
        I = data[IOI]
        I_low = _quantile(I, p_low)
        I_high = _quantile(I, p_high)

        # exclude the hypo- and hyper-intense outliers from consideration
        I = I[I >= I_low]; I = I[I <= I_high]

        # map the range of the current image to the desired range (i_min, i_max)
        I = i_min + (i_max-i_min)*(I-min(I))/(max(I)-min(I))

        # on the new image intensities, find the quantiles of interest
        for iq in range(1,nquantiles):
            Q[iq] += _quantile(I, qs[iq] - qs[0])

    # calculate the landmarks by averaging
    ref_deciles = Q/nCases

    # store the 'border' landmarks too
    ref_deciles[0] = i_min; ref_deciles[nquantiles] = i_max

    return ref_deciles, qs


def histogramMatchingFilter(imageFile,outputFile, ref_deciles, qs, bckg=0):
    # example of usage:
    # import SimpleITK as sitk
    # from pylab import *
    # %matplotlib inline
    # import preprocessor as prep
    # import json
    # import os
    #
    # pthOrig = 'C:/alfiia/projects/SU/BrainTumor/BRATS13/Training'
    # jsonFile = '%s/%s'%(pthOrig, 'quantileValues.json')
    # pthHMF = pthOrig + '_HMF4'
    #
    # bratsDB = prep.loadBRATS(pthOrig)
    #
    # for smod in ['T1','T1c','T2','FLAIR']:
    #     with open(jsonFile) as fp:
    #         trainedQ = json.load(fp)
    #         ref_deciles = trainedQ[smod]['ref_deciles']
    #         qs = trainedQ[smod]['qs']
    #     for stype in ['HGG','LGG']:
    #         for icase in range(len(bratsDB[stype])):
    #             imageFile = '%s/%s'%(pthOrig,bratsDB[stype][icase][smod])
    #             outputFile = '%s/%s'%(pthHMF,bratsDB[stype][icase][smod])
    #
    #             outputDir = os.path.dirname(outputFile)
    #             if not os.path.isdir(outputDir):
    #                 os.makedirs(outputDir)
    #             prep.histogramMatchingFilter(imageFile,outputFile,ref_deciles, qs)
    #
    inputImage = sitk.ReadImage(imageFile)
    img = np.float32(sitk.GetArrayFromImage(inputImage))

    nquantiles = len(ref_deciles)-1 # number of quantiles trained

    # the extreme values of the reference histograms
    i_min = ref_deciles[0]
    i_max = ref_deciles[nquantiles]

    IOI = img>min(img.flatten())
    I = img[IOI>0]

    imgNormalized = img # the storage for the output image

    # on the target image intensities, find the quantiles of interest
    Q = np.zeros(nquantiles+1)
    Q[0] = _quantile(I, qs[0])
    Q[nquantiles] = _quantile(I, qs[nquantiles])
    ind = (I>=Q[0]) & (I<=Q[nquantiles]) # outlier-free indices
    for iq in range(1,nquantiles+1):
        Q[iq] = _quantile(I[ind], qs[iq])

    I2 = np.zeros(I.shape) # storage for the normalized intensities

    ref_range = i_max - i_min
    img_range = ref_range
    prev = ref_deciles[0] # beginning of the previous interval

    # process the first interval and the leftmost tail (ie hypo-intense outliers)
    iq = 0
    ref_range_int = ref_deciles[iq+1]-ref_deciles[iq] # reference interval's range

    ind = (I>=Q[iq]) & (I<=Q[iq+1]) # target interval's indices
    I2[ind] = prev + ref_range_int*(I[ind]-min(I[ind]))/(max(I[ind])-min(I[ind]))


    # process the tail
    I2[I<Q[0]] = ref_deciles[0]
    # process the background
    imgNormalized[IOI<0] = bckg

    # go through all the middle intervals
    for iq in range(1, nquantiles):
        prev = ref_deciles[iq]
        ref_range_int = ref_deciles[iq+1]-ref_deciles[iq] # reference interval's range
        ind = (I>Q[iq]) & (I<=Q[iq+1])# target interval's indices
        I2[ind] = prev + (ref_range_int/ref_range)*img_range*(I[ind]-Q[iq])/(Q[iq+1]-Q[iq])

    ref_range_int = ref_deciles[nquantiles]-ref_deciles[nquantiles-1]
    #process the rightmost tail
    ind = I>Q[nquantiles]
    I2[ind] = ref_deciles[nquantiles]

    #  update the intensities
    imgNormalized[IOI>0] = I2

    outputImage = sitk.GetImageFromArray(imgNormalized)
    outputImage.CopyInformation(inputImage)

    sitk.WriteImage(outputImage, outputFile)


def _patchSampler(imageFile, s=2, bRandom=False):
    # example of usage:
    # chess = _patchSampler(imageFile,s=4,brandom=True)
    # _, ax = plt.subplots(1,3)
    # ax[0].imshow(chess[100,:20,:20],interpolation="nearest")
    # ax[1].imshow(chess[:40,100,:40],interpolation="nearest")
    # ax[2].imshow(chess[:40,:40,100],interpolation="nearest")
    #
    # chess = sitk.GetImageFromArray(chess)
    # sitk.WriteImage( chess,outputFile)

    inputImage = sitk.ReadImage(imageFile)
    data = sitk.GetArrayFromImage(inputImage)

    assert(s%2 == 0)

    if not bRandom:
        # regular grid
        chess = np.zeros(data.shape)
        for i in range(0,chess.shape[0],s):
            for j in range(0,chess.shape[1],s):
                for k in range(0,chess.shape[2],s):
                    chess[i,j,k] = 1

        for i in range(s/2,chess.shape[0],s):
            for j in range(s/2,chess.shape[1],s):
                for k in range(s/2,chess.shape[2],s):
                    chess[i,j,k] = 1
    else:
        # randomized stride
        I,J,K = data.shape
        I -= 1; J -= 1; K -=1
        chess = np.zeros(data.shape)
        for i in range(0,chess.shape[0],s):
            for j in range(0,chess.shape[1],s):
                for k in range(0,chess.shape[2],s):
                    ri = max(0,min(I,(randint(i-s/2+1,i+1))))
                    rj = max(0,min(J,(randint(j-s/2+1,j+1))))
                    rk = max(0,min(K,(randint(k-s/2+1,k+1))))
                    chess[ri,rj,rk] = 1

        for i in range(s/2,chess.shape[0],s):
            for j in range(s/2,chess.shape[1],s):
                for k in range(s/2,chess.shape[2],s):
                    ri = max(0,min(I,(randint(i-s/2+1,i+1))))
                    rj = max(0,min(J,(randint(j-s/2+1,j+1))))
                    rk = max(0,min(K,(randint(k-s/2+1,k+1))))
                    chess[ri,rj,rk] = 1

    return chess


def _patchExtractor(inputFile, GTFile, masktype='brainmask', stride=2, minclass=0.01):
    inputImage = sitk.ReadImage(inputFile)
    data = sitk.GetArrayFromImage(inputImage)
    if not masktype == 'all':
        GTImage = sitk.ReadImage(GTFile)
        maskGT = sitk.GetArrayFromImage(GTImage)
    if masktype == 'brainmask':
        chess = _patchSampler(GTFile,s=stride)
        chessNABS = _patchSampler(GTFile,s=4,bRandom=True)
        mask = (chess>0) & (maskGT>0) | ((chessNABS>0) & (maskGT==0) & (data>0))
    elif masktype == 'bbox':
        chess = _patchSampler(GTFile,s=2)
        [IZ,IR,IC]=np.where(maskGT>0)
        minZ = min(IZ)
        minR = min(IR)
        minC = min(IC)

        maxZ = max(IZ)
        maxR = max(IR)
        maxC = max(IC)

        bbox = np.zeros(maskGT.shape)
        bbox[range(minZ,maxZ),:,:]=1
        bbox[:,range(minR,maxR)]+=1
        bbox[:,:,range(minC,maxC)]+=1

        mask = (chess>0) & (bbox>2) & (data>0)
    elif masktype == 'all':#for segmentation test
        mask = (data>0)

    if not masktype == 'all':
        # remove the under-respresented:
        for lab in unique(maskGT.flatten()):
            if float(sum(maskGT == lab))/sum(maskGT>0) < minclass:
                mask[maskGT == lab] = 0

    return mask


def patchGenerator(listFile, patchSize, patchtype='axial', masktype='brainmask',outputFile='', maxratio=2):
    assert(not patchSize % 2 == 0)
    nChannels = 4 #'FLAIR','T1c','T2','T1'
    PS = patchSize/2
    imageFile = listFile['FLAIR']
    GTFile = listFile['GT']

    GT = sitk.ReadImage(listFile['GT'])
    GT = sitk.GetArrayFromImage(GT)

    mask = _patchExtractor(imageFile, GTFile, masktype)

    # balance the classes if wanted
    if maxratio>0:
        labs = unique(GT[mask>0])
        cards = dict.fromkeys(labs,0)
        for lab in labs:
            cards[lab]=sum(GT[mask>0]==lab)
        maxcard = min(cards.values())*maxratio
        for lab in labs:
            # delete the exceeding points randomly
            delcard = max(0,cards[lab]-maxcard)
            if delcard>0:
                inds = np.arange(cards[lab])
                shuffle(inds)
                inds = inds[:delcard]
                _mask = (GT==lab) & (mask>0)
                INDS = np.flatnonzero(_mask)
                mask[np.unravel_index(INDS[inds],_mask.shape)]=0

    [IZZ,IRR,ICC] = np.where(mask>0)
    npatch = size(IZZ)

    # if needed, record directly to the output file
    if not outputFile=='':
        import h5py
        with h5py.File(outputFile,'w') as patchFile:
            if patchtype in {'axial','coronal','sagittal','triplanar'}:
                _datashape = [npatch,nChannels,patchSize,patchSize]
            #elif patchtype=='triplanar':
            #    _datashape = [npatch,nChannels*3,patchSize,patchSize]
            elif patchtype=='3D':
                _datashape = [npatch,nChannels,patchSize,patchSize,patchSize]
            patches = patchFile.create_dataset('data', shape=_datashape,dtype='float32')
            _labelshape = [npatch]
            labels = patchFile.create_dataset('labels', shape=_labelshape,dtype='uint8')
    else:
        if patchtype=='axial' or patchtype=='coronal' or patchtype=='sagittal':
            patches = np.zeros(shape=(npatch,nChannels,patchSize, patchSize))
        elif patchtype=='triplanar':
            #patches = np.zeros(shape=(npatch,nChannels*3,patchSize, patchSize))
            patchesA = np.zeros(shape=(npatch,nChannels,patchSize, patchSize))
            patchesC = np.zeros(shape=(npatch,nChannels,patchSize, patchSize))
            patchesS = np.zeros(shape=(npatch,nChannels,patchSize, patchSize))
        elif patchtype=='3D':
            patches = np.zeros(shape=(npatch,nChannels,patchSize, patchSize,patchSize))

    imageChannelCount = 0
    for smod in {'FLAIR','T1c','T2','T1'}:
        inputImage = sitk.ReadImage(listFile[smod])
        nda = np.float32(sitk.GetArrayFromImage(inputImage))
        # compute z-scores ('standardization')
        ind = np.where(nda>0)
        nda[ind] = (nda[ind] - np.mean(nda[ind]))/(np.std(nda[ind])+np.finfo(float).eps)
        nda = np.lib.pad(nda,((PS,PS),(PS,PS),(PS,PS)), 'constant',constant_values=(0,0))
        for ipatch in range(npatch):
            z = IZZ[ipatch]+PS; i = IRR[ipatch]+PS; j = ICC[ipatch]+PS
            if patchtype=='axial':
                patches[ipatch,imageChannelCount,:,:] = nda[z,i-PS:i+PS+1,j-PS:j+PS+1]
            elif patchtype=='coronal':
                patches[ipatch,imageChannelCount,:,:] = nda[z-PS:z+PS+1,i,j-PS:j+PS+1]
            elif patchtype=='sagittal':
                patches[ipatch,imageChannelCount,:,:] = nda[z-PS:z+PS+1,i-PS:i+PS+1,j]
            elif patchtype=='triplanar':
                patchesA[ipatch,imageChannelCount,:,:] = nda[z,i-PS:i+PS+1,j-PS:j+PS+1]
                patchesC[ipatch,imageChannelCount,:,:] = nda[z-PS:z+PS+1,i,j-PS:j+PS+1]
                patchesS[ipatch,imageChannelCount,:,:] = nda[z-PS:z+PS+1,i-PS:i+PS+1,j]
                #patches[ipatch,imageChannelCount*3,:,:] = nda[z,i-PS:i+PS+1,j-PS:j+PS+1]
                #patches[ipatch,imageChannelCount*3+1,:,:] = nda[z-PS:z+PS+1,i,j-PS:j+PS+1]
                #patches[ipatch,imageChannelCount*3+2,:,:] = nda[z-PS:z+PS+1,i-PS:i+PS+1,j]
            elif patchtype=='3D':
                patches[ipatch,imageChannelCount,:,:,:] = nda[z-PS:z+PS+1,i-PS:i+PS+1,j-PS:j+PS+1]

            ipatch +=1
        imageChannelCount += 1

    labels = GT[mask>0]
    if patchtype=='triplanar':
        return patchesA,patchesC,patchesS, labels, mask
    else:
        return patches, labels, mask