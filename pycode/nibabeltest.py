from __future__ import print_function
import matplotlib.pylab as plt
import h5py
import os
import numpy as np
import nibabel as nib
from preprocessor import _patchSampler, _patchExtractor
import re
import SimpleITK as sitk
import numpy as np
from pylab import *

def patchGenerator(listFile, patchSize, masktype='brainmask',outputFile='', maxratio=2):
    assert(not patchSize % 2 == 0)
    nChannels = channels #'T1','T2','FLAIR'
    PS = patchSize/2
    imageFile = listFile['S1_FLAIR']
    GTFile = listFile['GT']

    GT = sitk.ReadImage(listFile['GT'])
    GT = sitk.GetArrayFromImage(GT)

    mask = _patchExtractor(imageFile, GTFile, masktype)
    # print(unique(GT[mask]))

    # balance the classes if wanted
    if maxratio>0:
        labs = unique(GT[mask])
        cards = dict.fromkeys(labs,0)
        for lab in labs:
            cards[lab]=sum(GT[mask>0]==lab)
        maxcard = min(cards.values())*maxratio
        for lab in labs:
            # delete the exceeding points randomly
            delcard = max(0,cards[lab]-maxcard)
            if delcard>0:
                inds = np.arange(cards[lab])
                shuffle(inds)
                inds = inds[:delcard]
                _mask = (GT==lab) & (mask>0)
                INDS = np.flatnonzero(_mask)
                mask[np.unravel_index(INDS[inds],_mask.shape)]=0



    [IZZ,IRR,ICC] = np.where(mask>0)
    npatch = size(IZZ)

    # if needed, record directly to the output file
    if not outputFile=='':
        import h5py
        with h5py.File(outputFile,'w') as patchFile:
            _datashape = [npatch,nChannels,patchSize,patchSize]
            patches = patchFile.create_dataset('data', shape=_datashape,dtype='float32')
            _labelshape = [npatch]
            labels = patchFile.create_dataset('labels', shape=_labelshape,dtype='uint8')
    else:
        patches = np.zeros(shape=(npatch,nChannels,patchSize, patchSize))
        

    imageChannelCount = 0
    for smod in {'S1_T1','S2_T1','S1_FLAIR'}:
        timepoints = [smod,'S2_%s'%(smod[3:])]
        ndas = []
        for name in timepoints:
            inputImage = sitk.ReadImage(listFile[name])
            nda = np.float32(sitk.GetArrayFromImage(inputImage))
            # compute z-scores ('standardization')
            ind = np.where(name>0)
            nda[ind] = (nda[ind] - np.mean(nda[ind]))/(np.std(nda[ind])+np.finfo(float).eps)
            nda = np.lib.pad(nda,((PS,PS),(PS,PS),(PS,PS)), 'constant',constant_values=(0,0))
            ndas.append(nda)
        dist = ndas[1] - ndas[0]
        for ipatch in range(npatch):
            z = IZZ[ipatch]+PS; i = IRR[ipatch]+PS; j = ICC[ipatch]+PS
            patches[ipatch,imageChannelCount,:,:] = dist[z,i-PS:i+PS+1,j-PS:j+PS+1]
            

            ipatch +=1
        imageChannelCount += 1

    labels = GT[mask]

    return patches, labels, mask


channels = 3
dataDir = '../../MSpatientdata'
# subjectID = 1
subjectIDs = [1, 2, 3, 4]
images = []
lbls = []
patchSizes = [29]
for subjectID in subjectIDs:
    listFile = {}
    filename = {}
    filename['S1_T1'] = 'study1_T1Wreg'
    filename['S1_T2'] = 'study1_T2Wreg'
    filename['S2_T1'] = 'study2_T1Wreg'
    filename['S2_T2'] = 'study2_T2Wreg'
    filename['S1_FLAIR'] = 'study1_FLAIRreg'
    filename['S2_FLAIR'] = 'study2_FLAIRreg'
    filename['GT'] = 'gt3'
    filename['brainmask'] = 'brainmask'
    for smod in {'S1_T1','S1_T2','S2_T1','S2_T2','S1_FLAIR', 'S2_FLAIR', 'GT','brainmask'}:
        listFile[smod] = '%s/patient%d/patient%d_%s.nii.gz'%(dataDir, subjectID, subjectID, filename[smod])



    for pSize in patchSizes:
        patches, labels, mask = patchGenerator(listFile, pSize, masktype='brainmask',outputFile='', maxratio=2)


        # store 
        
        outputDir = 'NAME'
        outputFile = '%s/patient%s_TrainingPatches%d.hdf5'%(outputDir, subjectID, pSize)
        print('creating '+outputFile)
        if not os.path.isdir(outputDir):
            os.makedirs(outputDir)
        with h5py.File(outputFile,'w') as patchFile:
            _patches = patchFile.create_dataset('data', data = patches,dtype='float32',compression="gzip")
            _labels = patchFile.create_dataset('labels', data = labels,dtype='uint8',compression="gzip")
from __future__ import print_function
import keras
from keras.datasets import mnist
from keras.layers import Dense, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.models import Sequential
import matplotlib.pylab as plt
import h5py
import os
import numpy as np
import nibabel as nib
from preprocessor import _patchSampler, _patchExtractor
import re
import SimpleITK as sitk
import numpy as np
from pylab import *

channels = 3
dataDir = '../../MSpatientdata'
# subjectID = 1
subjectIDs = [1, 2, 3, 4]
images = []
lbls = []
outputDir = 'NAME'
patchSizes = [29]
for subjectID in subjectIDs:
        # read
    for pSize in patchSizes:

        outputFile = '%s/patient%s_TrainingPatches%d.hdf5'%(outputDir, subjectID, pSize)
        with h5py.File(outputFile,'r') as patchFile:
            images.append(patchFile['data'][()])
            x = 0
            # plt.imshow(images[x,:,:],interpolation="nearest", cmap = 'gray')
            lbls.append(patchFile['labels'][()])
            print(patchFile.get('labels'))

batch_size = 128
num_classes = 3
epochs = 10

# input image dimensions
img_x, img_y = patchSizes[0], patchSizes[0]
imarr = np.array(images[0])
lblarr = np.array(lbls[0])
trainIDs = subjectIDs[1:-1]
for trainID in trainIDs:
    print(trainID)
    imarr = np.concatenate([imarr, images[trainID-1]])
    lblarr = np.concatenate([lblarr, lbls[trainID-1]])
print(imarr.shape)
x_train, x_test = imarr, np.array(images[-1])
y_train, y_test = lblarr, np.array(lbls[-1])

print(x_train.shape)

# reshape the data into a 4D tensor - (sample_number, x_img_size, y_img_size, num_channels)
x_train = x_train.reshape(x_train.shape[0], img_x, img_y, channels)
x_test = x_test.reshape(x_test.shape[0], img_x, img_y, channels)
input_shape = (img_x, img_y, channels)

# convert the data to the right type
x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255
print('x_train shape:', x_train.shape)
print(x_train.shape[0], 'train samples')
print(x_test.shape[0], 'test samples')


# convert class vectors to binary class matrices - this is for use in the
# categorical_crossentropy loss below
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

model = Sequential()
model.add(Conv2D(32, kernel_size=(5, 5), strides=(1, 1),
                 activation='relu',
                 input_shape=input_shape))
model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
model.add(Conv2D(64, (5, 5), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Flatten())
model.add(Dense(1000, activation='relu'))
model.add(Dense(num_classes, activation='softmax'))

#default: keras.optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer=keras.optimizers.Adam(0.001),
              metrics=['accuracy'])

#new

class AccuracyHistory(keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.acc = []


    def on_epoch_end(self, batch, logs={}):
        self.acc.append(logs.get('acc'))
        print(self.acc)

hist = AccuracyHistory()

history = model.fit(x_train, y_train,
          batch_size=batch_size,
          epochs=epochs,
          verbose=1,
          validation_data=(x_test, y_test),
          callbacks=[hist])
score = model.evaluate(x_test, y_test, verbose=0)
print('Test loss:', score[0])
print('Test accuracy:', score[1])

#accuracy
# plt.plot(range(1, 11), history.acc)
# plt.xlabel('Epochs')
# plt.ylabel('Accuracy')
# plt.show()


#new accuracy
print(history.history.keys())
# summarize history for accuracy
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()
# # summarize history for loss
# plt.plot(history.history['loss'])
# plt.plot(history.history['val_loss'])
# plt.title('model loss')
# plt.ylabel('loss')
# plt.xlabel('epoch')
# plt.legend(['train', 'test'], loc='upper left')
# plt.show()
    
