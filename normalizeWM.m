function [nt1, nt2, nflair] = normalizeWM(t1, t2, flair, mask)
% normalize the MR intensities according to WM estimates
feat = [t1(mask>0),t2(mask>0),flair(mask>0)]';
feat = double(feat);
initseg = otsu(t1,3,mask>0);
opts = struct;
pts.maxiter_gmm = 10;
%%
for itissue = 1:3
    opts.init.Mu(:,itissue) = median( feat(:,initseg(mask)==itissue),2 ) ;
    opts.init.Sigma(:,:,itissue) = eye(3)*10;
    opts.init.Priors(itissue) = nnz(initseg(mask)==itissue)/nnz(mask);
end
%%
vdata = RMLE_GMM( feat, opts )
%%
mu_WM = vdata.Mu(:,1);
sigma_WM = sqrt(diag(vdata.Sigma(:,:,1)));
%%
nt1 = zeros(size(t1));
nt1(mask) = (t1(mask) - mu_WM(1))/sigma_WM(1);

nt2 = zeros(size(t2));
nt2(mask) = (t2(mask) - mu_WM(2))/sigma_WM(2);

nflair = zeros(size(flair));
nflair(mask) = (flair(mask) - mu_WM(3))/sigma_WM(3);
