
function [pxOmega,priorsOmega] = pdfMixture(feat, vdata, patchWeight, opts)
%feat is Dim*Elements

%--------------------------------------------------------------------------
% check input args
if nargin<5, opts = struct; end

% fill missing parameters with default values

% if ~isfield(opts,'pmaha'), opts.pmaha = 0.3; end % p-value on mahalanobis statistic (Hi-Sens!, Lo-Spec!)
% if ~isfield(opts,'phyper'), opts.phyper = 1; end % p-value on normal statistic (1-disabled) to detect T2 lesions
if ~isfield(opts,'verbose'), opts.verbose = 1; end % 0-disabled, >0-enabled

level = size(vdata,2); % only for one level
subNums = size(vdata{level}, 2);

[dim, featNum] = size(feat);
if ~isfield(opts,'nclust')
    classNum = 3;
else
    classNum =opts.nclust;
end
p = zeros(featNum,classNum);%
pxOmega = p;

for i = 1:classNum
    denom = 0;
    Pi = 0;%
    for subNum = 1:subNums
        Mu = vdata{level}{subNum}.tle.Mu(:,i);
        Sigma = vdata{level}{subNum}.tle.Sigma(:,:,i);
        Pji = vdata{level}{subNum}.tle.Priors(:,i);
        
        if ~isnan(Pji)&&Pji>0
            PiPatch = patchWeight{level}{subNum};
            Pi =  Pi + PiPatch * Pji;%
            
                if det(Sigma)<0
                    Sigma = Sigma+eye(3)*1e-03;
                end
                
            p(:,i)  = p(:,i) +  Pji *  PiPatch * mvnpdf(feat', Mu', Sigma);
            denom = denom + Pji * PiPatch;
        end
    end
    pxOmega(:,i) = p(:,i)/denom;%;
    %     p(:,i) = p(:,i);
    if nargout>1
        priorsOmega(:,i) = denom;
    end
end
%denominate the prior for j (Pji*Pj(SUM_j(Pji*Pj)))
%p = p/denom;

end

