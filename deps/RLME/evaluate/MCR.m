
function f = MCR(Seg, Ref)
% compute the misclassification ratio
% input:
%   Seg - the output of the classification method to be evaluated
%   Ref - reference classification of the same size as Seg

labels = unique(Ref);
TP = 0;
for lab = labels(:)'
    TP = TP + nnz(Seg==lab & Ref==lab);%numel(intersect(find(Seg==lab),find(Ref==lab)));
end
total = numel(Ref);
f = (total-TP)/total;
end