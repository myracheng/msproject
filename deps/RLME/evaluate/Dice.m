
function dice = Dice(SegBW,RefBW)

% s_ind = find(SegBW > 0);
% r_ind = find(RefBW > 0);
% TP = numel(intersect(s_ind, r_ind));
% 
% dice = 2*TP / (numel(s_ind) + numel(r_ind));

dice = 2*(nnz(SegBW & RefBW))/(nnz(SegBW)+nnz(RefBW));

end