function p = pdfGMM(data, Mu, Sigma, Priors)
[N, dim] = size(data);
nlab = size(Mu,2);
p = zeros(N,1);
for lab = 1:nlab
    p = p + Priors(:,lab).*mvnpdf(data, Mu(:,lab)', Sigma(:,:,lab));
end