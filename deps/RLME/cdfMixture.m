function [thr, T] = cdfMixture(feat, vdata, patchWeight, pvalue, classNum, nclust,bdigital)
% thr : threshold for p(x|omega_i)
% T is a struct, logging data for the p(x|omega_i):
%    T.BW : binary mask for the computed threshold thr
%    T.xx, T.yy, T.zz : axis that were user for cdf computation

if nargin<6
    nclust = 3;
end
if nargin<7
    bdigital = false;
end

level = size(vdata,2); % only for one level
subNums = size(vdata{level}, 2);
% classNum = 3;
[dim, featNum] = size(feat);

if (dim>2)
    %figure out the boundaries for the integration
    conf =  min(1-eps, max(0.9974, max(pvalue,1-pvalue/2)));
    % conf =  min(1-eps, max(pvalue,1-pvalue/2));
    dthr = sqrt(qchisq(conf,dim));
    
    maxX = -inf;
    maxY = -inf;
    maxZ = -inf;
    minX = inf;
    minY = inf;
    minZ = inf;
    
    for subNum = 1:subNums
        Mu = vdata{level}{subNum}.tle.Mu(:,classNum);
        Sigma = vdata{level}{subNum}.tle.Sigma(:,:,classNum);
        Priors = vdata{level}{subNum}.tle.Priors(classNum);
        % Mu = zeros([3,1]);
        % Sigma = eye(3);
        %   supr = max(Mu)+50;
        %   infm = min(Mu)-50;
        if ~isnan(Mu(1))
            x0 = Mu(1);
            y0 = Mu(2);
            z0 = Mu(3);
            
            Cxy = Sigma(1:2,1:2);
            Cyz = Sigma(2:3,2:3);
            Czx = Sigma([3 1],[3 1]);
            
            [x,y,z] = getpoints(Cxy);
            maxX = max([maxX, max(x)*dthr+x0]);
            minX = min([minX, min(x)*dthr+x0]);
            
            [y,z,x] = getpoints(Cyz);
            maxY = max([maxY, max(y)*dthr+y0]);
            minY = min([minY, min(y)*dthr+y0]);
            
            [z,x,y] = getpoints(Czx);
            maxZ = max([maxZ, max(z)*dthr+z0]);
            minZ = min([minZ, min(z)*dthr+z0]);
        end
    end
    
    % ensure the features are captured within the boundaries    
    maxX = max(maxX, max(feat(1,:)));
    minX = min(minX, min(feat(1,:)));
    maxY = max(maxY, max(feat(2,:)));
    minY = min(minY, min(feat(2,:)));
    maxZ = max(maxZ, max(feat(3,:)));
    minZ = min(minZ, min(feat(3,:)));
    
    %     % compare to mahalanobis-distance-based threshold
    %     k = sqrt(qchisq(1-pvalue,dim));
    %     [eigvec,eigval] = eig(Sigma);
    %     A = [0,0,1]*sqrt(eigval)*eigvec';
    %     A(1) = (k*A(:,1)+x0);
    %     A(2) = (k*A(:,2)+y0);
    %     A(3) = (k*A(:,3)+z0);
    %     B = [0,1,0]*sqrt(eigval)*eigvec'
    %     B(1) = (k*B(:,1)+x0);
    %     B(2) = (k*B(:,2)+y0);
    %     B(3) = (k*B(:,3)+z0);
    %     C = [1,0,0]*sqrt(eigval)*eigvec';
    %     C(1) = (k*C(:,1)+x0);
    %     C(2) = (k*C(:,2)+y0);
    %     C(3) = (k*C(:,3)+z0);
    %     mvnpdf(A, Mu', Sigma)
    %     mvnpdf(B, Mu', Sigma)
    %     mvnpdf(C, Mu', Sigma)
    
    % calculate the probabilities on the feature set
    p = zeros(featNum,nclust);%
    pxOmega = p;
    
    for i = 1:nclust%classNum
        denom = 0;
        Pi = 0;%
        for subNum = 1:subNums
            Mu = vdata{level}{subNum}.tle.Mu(:,i);
            Sigma = vdata{level}{subNum}.tle.Sigma(:,:,i);
            Pji = vdata{level}{subNum}.tle.Priors(:,i);
            
            if ~isnan(Pji)&&Pji>0
                PiPatch = patchWeight{level}{subNum};
                Pi =  Pi + PiPatch * Pji;%
                
                p(:,i)  = p(:,i) +  Pji *  PiPatch * mvnpdf(feat', Mu', Sigma);
                denom = denom + Pji * PiPatch;
            end
        end
        pxOmega(:,i) = p(:,i)/denom;%;
        %     p(:,i) = p(:,i);
    end
    T = struct;
    T.feat_pdf = pxOmega;
    
% calculate the cdf's on the mesh
    sumMxSort_old = 0;
    n = 100;
    
    %  for subNum = 1:subNums
    %
    %  end
    
    
    ind = 0;
    for ii = 1:numel(patchWeight{2})
        mu = vdata{level}{ii}.tle.Mu;
        if ~isequal(mu, zeros(size(mu)))
            ind = ind+1;
            tPw{2}{ind} = patchWeight{2}{ii};
            tVdata{2}{ind}.tle.Mu = vdata{level}{ii}.tle.Mu;
            tVdata{2}{ind}.tle.Sigma = vdata{level}{ii}.tle.Sigma;
            tVdata{2}{ind}.tle.Priors = vdata{level}{ii}.tle.Priors;
        end
    end
    patchWeight =  tPw;
    vdata = tVdata;
    
    subNums = size(vdata{level}, 2);
    
    % initialize the grid
    dx = abs(maxX-minX)/n;
    dy = abs(maxY-minY)/n;
    dz = abs(maxZ-minZ)/n;
    
    precise = false;
    while ~precise 
        
        [xx,yy,zz] = meshgrid(minX:dx:maxX, minY:dy:maxY, minZ:dz:maxZ);
        
        % re-normalize the patch weights (they overlap at box boundaries)
        sumPj = 0;
        for ii = 1:numel(patchWeight{2})
            sumPj = patchWeight{2}{ii} + sumPj;
        end
        for ii = 1:numel(patchWeight{2})
            patchWeight{2}{ii} = patchWeight{2}{ii}/sumPj;
        end
        
        %integrate the sorted grid-sample
        p_i = zeros(size(xx(:),1),1);
        
        denom = 0;
        
        for subNum = 1:subNums
            Mu = vdata{level}{subNum}.tle.Mu(:,classNum);
            Sigma = vdata{level}{subNum}.tle.Sigma(:,:,classNum);
            %------------------------------------------------------------------
            %         [u,s,v] = svd(Sigma);
            Pj = patchWeight{level}{subNum};
            Pji = vdata{level}{subNum}.tle.Priors(:,classNum);
            
            if ~isnan(Pji)&&Pji>0
% %                 [u,s] = eig(Sigma);
% %                 changed = false;
% %                 if s(1,1)<dx
% %                     s(1,1) = dx;
% %                     changed = true;
% %                 end
% %                 if s(2,2)<dy
% %                     s(2,2) = dy;
% %                     changed = true;
% %                 end
% %                 if s(3,3)<dz
% %                     s(3,3) = dz;
% %                     changed = true;
% %                 end
% %                 
% %                 if changed
% %                     Sigma = u*s*inv(u);
% %                     %det(Sigma)
% %                     %Sigma = ceil(Sigma)
% %                     Sigma(2,1) = (Sigma(2,1)+Sigma(1,2))/2;
% %                     Sigma(1,2) = Sigma(2,1);
% %                     Sigma(3,1) = (Sigma(3,1)+Sigma(1,3))/2;
% %                     Sigma(1,3) = Sigma(3,1);
% %                     Sigma(3,2) = (Sigma(3,2)+Sigma(2,3))/2;
% %                     Sigma(2,3) = Sigma(3,2);
% %                     Sigma;
% %                 end
                
                % %         dm = max([dx,dy,dz]);
                % %         if s(1,1)<dx || s(2,2)<dy || s(3,3)<dz
                % %             s(1,1) = dm;
                % %             s(2,2) = dm;
                % %             s(3,3) = dm;
                % %             Sigma = s;%u*s*v';
                % %         end
                
                %------------------------------------------------------------------
                
                p_i  = p_i +  Pji * Pj * mvnpdf([xx(:), yy(:), zz(:)],Mu', Sigma);%
                denom = denom + Pji * Pj;
            end
        end %j
        
        %denominate the prior for j (Pji*Pj(SUM_j(Pji*Pj)))
         p_i = p_i/denom;
        denom;
        
        P_i = reshape(p_i, size(xx));
        Mx = p_i* dx*dy*dz;%
        [MxSort, ind_sort] = sort(Mx(:), 'descend');%ascend
        
        precise = (sum(MxSort)-sumMxSort_old)<pvalue/2;%/2
        %precise = (sum(MxSort)-conf)<pvalue/10;
        if bdigital
            precise = precise || dx<1 || dy<1 || dz<1;
        end
        if ~precise
            sumMxSort_old = sum(MxSort);
            n = n*1.5;
            
            dx = abs(maxX-minX)/n;
            dy = abs(maxY-minY)/n;
            dz = abs(maxZ-minZ)/n;
        end
    end
    if isempty(MxSort)
        thr = NaN;
        T = NaN;
        return
    end
    n;
    sum(MxSort);
    sz = numel(MxSort);
    s = 0;
    j = 0;
    conf = (1-pvalue);%sum(MxSort)*
    while(s <= conf && j<sz)
        j = j+1;
        s = s + MxSort(j) ;
    end
    
    if (j>=sz)
        classNum;
        errordlg('wrong pvalue or smth');
    end
    
    thr = MxSort(j)/(dx*dy*dz);
    
    if nargout>1
        %  output log
        mesh_cdf_ = cumsum(MxSort);
        mesh_cdf(ind_sort) = mesh_cdf_;
        T.xx = xx;
        T.yy = yy;
        T.zz = zz;
        T.Mx = reshape(mesh_cdf, size(xx));
        T.pdf = P_i;%T.Mx/(dx*dy*dz);
        T.MxSort = MxSort;
        T.pdfSort = MxSort/(dx*dy*dz);
        %  create the binary mask for thresholded  p(x|omega_i)
        BW = zeros(size(P_i));
        ind = find(P_i(:) >= thr);
        BW(ind) = 1;
        T.BW = BW;
    end
elseif (dim>1)
    %figure out boundaries for the integration
    conf =  min(1-eps, max(0.9974, 1-pvalue/5));
    dthr = sqrt(qchisq(conf,dim));
    
    maxX = -inf;
    maxY = -inf;
    minX = inf;
    minY = inf;
        
    for subNum = 1:subNums
        Mu = vdata{level}{subNum}.tle.Mu(:,classNum);
        Sigma = vdata{level}{subNum}.tle.Sigma(:,:,classNum);
        Priors = vdata{level}{subNum}.tle.Priors(classNum);
        % Mu = zeros([3,1]);
        % Sigma = eye(3);
        %   supr = max(Mu)+50;
        %   infm = min(Mu)-50;
        if ~isnan(Mu(1))
            x0 = Mu(1);
            y0 = Mu(2);
                        
            Cxy = Sigma(1:2,1:2);
                        
            [x,y] = getpoints(Cxy);
            maxX = max([maxX, max(x)*dthr+x0]);
            minX = min([minX, min(x)*dthr+x0]);
            maxY = max([maxY, max(y)*dthr+y0]);
            minY = min([minY, min(y)*dthr+y0]);
        end
    end
    % ensure the features are captured within the boundaries    
    maxX = max(maxX, max(feat(1,:)));
    minX = min(minX, min(feat(1,:)));
    maxY = max(maxY, max(feat(2,:)));
    minY = min(minY, min(feat(2,:)));
    % calculate the probabilities on the feature set
    p = zeros(featNum,nclust);%
    pxOmega = p;
    
    for i = 1:nclust%classNum
        denom = 0;
        Pi = 0;%
        for subNum = 1:subNums
            Mu = vdata{level}{subNum}.tle.Mu(:,i);
            Sigma = vdata{level}{subNum}.tle.Sigma(:,:,i);
            Pji = vdata{level}{subNum}.tle.Priors(:,i);
            
            if ~isnan(Pji)
                PiPatch = patchWeight{level}{subNum};
                Pi =  Pi + PiPatch * Pji;%
                
                p(:,i)  = p(:,i) +  Pji *  PiPatch * mvnpdf(feat', Mu', Sigma);
                denom = denom + Pji * PiPatch;
            end
        end
        pxOmega(:,i) = p(:,i)/denom;%;
        %     p(:,i) = p(:,i);
    end
    T = struct;
    T.feat_pdf = pxOmega;
    
% calculate the cdf's on the mesh
    sumMxSort_old = 0;
    n = 100;
    
    %  for subNum = 1:subNums
    %
    %  end
    
    
    ind = 0;
    for ii = 1:numel(patchWeight{2})
        mu = vdata{level}{ii}.tle.Mu;
        if ~isequal(mu, zeros(size(mu)))
            ind = ind+1;
            tPw{2}{ind} = patchWeight{2}{ii};
            tVdata{2}{ind}.tle.Mu = vdata{level}{ii}.tle.Mu;
            tVdata{2}{ind}.tle.Sigma = vdata{level}{ii}.tle.Sigma;
            tVdata{2}{ind}.tle.Priors = vdata{level}{ii}.tle.Priors;
        end
    end
    patchWeight =  tPw;
    vdata = tVdata;
    
    subNums = size(vdata{level}, 2);
    
    precise = false;
    while ~precise
        % initialize the grid
        
        % dx = max([abs(maxX-minX), abs(maxY-minY), abs(maxZ-minZ)])/n;
        dx = abs(maxX-minX)/n;
        dy = abs(maxY-minY)/n;
       
        
        [xx,yy] = meshgrid(minX:dx:maxX, minY:dy:maxY);
        
        % re-normalize the patch weights (they overlap at box boundaries)
        sumPj = 0;
        for ii = 1:numel(patchWeight{2})
            sumPj = patchWeight{2}{ii} + sumPj;
        end
        for ii = 1:numel(patchWeight{2})
            patchWeight{2}{ii} = patchWeight{2}{ii}/sumPj;
        end
        
        %integrate the sorted grid-sample
        p_i = zeros(size(xx(:),1),1);
        
        denom = 0;
        
        for subNum = 1:subNums
            Mu = vdata{level}{subNum}.tle.Mu(:,classNum);
            Sigma = vdata{level}{subNum}.tle.Sigma(:,:,classNum);
            %------------------------------------------------------------------
            %         [u,s,v] = svd(Sigma);
            Pj = patchWeight{level}{subNum};
            Pji = vdata{level}{subNum}.tle.Priors(:,classNum);
            
            if ~isnan(Pji)
                [u,s] = eig(Sigma);
                changed = false;
                if s(1,1)<dx
                    s(1,1) = dx;
                    changed = true;
                end
                if s(2,2)<dy
                    s(2,2) = dy;
                    changed = true;
                end
                                
                if changed
                    Sigma = u*s*inv(u);
                    %det(Sigma)
                    %Sigma = ceil(Sigma)
                    Sigma(2,1) = (Sigma(2,1)+Sigma(1,2))/2;
                    Sigma(1,2) = Sigma(2,1);
                    
                end
                
                % %         dm = max([dx,dy,dz]);
                % %         if s(1,1)<dx || s(2,2)<dy || s(3,3)<dz
                % %             s(1,1) = dm;
                % %             s(2,2) = dm;
                % %             s(3,3) = dm;
                % %             Sigma = s;%u*s*v';
                % %         end
                
                %------------------------------------------------------------------
                
                p_i  = p_i +  Pji * Pj * mvnpdf([xx(:), yy(:)],Mu', Sigma);%
                denom = denom + Pji * Pj;
            end
        end %j
        
        %denominate the prior for j (Pji*Pj(SUM_j(Pji*Pj)))
        p_i = p_i/denom;
        denom;
        
        P_i = reshape(p_i, size(xx));
        Mx = p_i* dx*dy;%
        [MxSort, ind_sort] = sort(Mx(:), 'descend');%ascend
        
        precise = (sum(MxSort)-sumMxSort_old)<pvalue/2;
        sumMxSort_old = sum(MxSort);
        n = n*1.5;
    end
    if isempty(MxSort)
        thr = NaN;
        T = NaN;
        return
    end
    n;
    sum(MxSort);
    sz = numel(MxSort);
    s = 0;
    j = 0;
    conf = sum(MxSort)*(1-pvalue);
    while(s <= conf && j<sz)
        j = j+1;
        s = s + MxSort(j) ;
    end
    
    if (j>=sz)
        classNum;
        errordlg('wrong pvalue or smth');
    end
    
    thr = MxSort(j)/(dx*dy);
    
    if nargout>1
        %  output log
        mesh_cdf_ = cumsum(MxSort);
        mesh_cdf(ind_sort) = mesh_cdf_;
        T.xx = xx;
        T.yy = yy;
        T.Mx = reshape(mesh_cdf, size(xx));
        T.MxSort = MxSort;
        T.pdfSort = MxSort/(dx*dy);
        %  create the binary mask for thresholded  p(x|omega_i)
        BW = zeros(size(P_i));
        ind = find(P_i(:) >= thr);
        BW(ind) = 1;
        T.BW = BW;
    end
    %  px = zeros(featNum,1);
    %  for i = 1:featNum
    %     px(i) = Mx(feat(1,i), feat(2,i), feat(3,i));
    %  end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %                       one-dimentional data
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
else % for 1-d data
    % handle the one-channel case
    %figure out the boundaries for the integration
    conf =  min(1-eps, min(0.9974, 1-pvalue/5));
    dthr = sqrt(qchisq(conf,dim));
    
    maxX = -inf;
    
    minX = inf;
    
     % calculate the probabilities on the feature set
    p = zeros(featNum,nclust);%
    pxOmega = p;
    
    for i = 1:nclust%classNum
        denom = 0;
        Pi = 0;%
        for subNum = 1:subNums
            Mu = vdata{level}{subNum}.tle.Mu(:,i);
            Sigma = vdata{level}{subNum}.tle.Sigma(:,:,i);
            Pji = vdata{level}{subNum}.tle.Priors(:,i);
            
            if ~isnan(Pji)
                PiPatch = patchWeight{level}{subNum};
                Pi =  Pi + PiPatch * Pji;%
                
                %p(:,i)  = p(:,i) +  Pji *  PiPatch * gaussPDF(feat', Mu', Sigma);
                p(:,i)  = p(:,i) +  Pji *  PiPatch * pdf('normal', feat', Mu, sqrt(Sigma));
                denom = denom + Pji * PiPatch;
            end
        end
        pxOmega(:,i) = p(:,i)/denom;%;
        %     p(:,i) = p(:,i);
    end
    T = struct;
    T.feat_pdf = pxOmega;
    
    
    for subNum = 1:subNums
        Mu = vdata{level}{subNum}.tle.Mu(:,classNum);
        Sigma = vdata{level}{subNum}.tle.Sigma(:,:,classNum);
        % Mu = zeros([3,1]);
        % Sigma = eye(3);
        %   supr = max(Mu)+50;
        %   infm = min(Mu)-50;
        
        x0 = Mu(1);
        
        x = [-sqrt(Sigma),sqrt(Sigma)];
        
        maxX = max([maxX, max(x)*dthr+x0]);
        minX = min([minX, min(x)*dthr+x0]);
        
    end
    % ensure the features are captures within the boundaries    
    maxX = max(maxX, max(feat(1,:)));
    minX = min(minX, min(feat(1,:)));
    
    sumMxSort_old = 0;
    n = 100;
    
    ind = 0;
    for ii = 1:numel(patchWeight{2})
        mu = vdata{level}{ii}.tle.Mu;
        if ~isequal(mu, zeros(size(mu)))
            ind = ind+1;
            tPw{2}{ind} = patchWeight{2}{ii};
            tVdata{2}{ind}.tle.Mu = vdata{level}{ii}.tle.Mu;
            tVdata{2}{ind}.tle.Sigma = vdata{level}{ii}.tle.Sigma;
            tVdata{2}{ind}.tle.Priors = vdata{level}{ii}.tle.Priors;
        end
    end
    patchWeight =  tPw;
    vdata = tVdata;
    
    subNums = size(vdata{level}, 2);
    
    precise = false;
    while ~precise
        % initialize the grid
        
        % dx = max([abs(maxX-minX), abs(maxY-minY), abs(maxZ-minZ)])/n;
        dx = abs(maxX-minX)/n;
        
        
        xx = minX:dx:maxX;
        
        % re-normalize the patch weights (they overlap at box boundaries)
        sumPj = 0;
        for ii = 1:numel(patchWeight{2})
            sumPj = patchWeight{2}{ii} + sumPj;
        end
        for ii = 1:numel(patchWeight{2})
            patchWeight{2}{ii} = patchWeight{2}{ii}/sumPj;
        end
        
        %integrate the sorted grid-sample
        p_i = zeros(size(xx(:),1),1);
        
        denom = 0;
        
        for subNum = 1:subNums
            Mu = vdata{level}{subNum}.tle.Mu(:,classNum);
            Sigma = vdata{level}{subNum}.tle.Sigma(:,:,classNum);

            Pj = patchWeight{level}{subNum};
            Pji = vdata{level}{subNum}.tle.Priors(:,classNum);
            if ~isnan(Pji)
                p_i  = p_i +  Pji * Pj * pdf('normal', xx(:),Mu', sqrt(Sigma));
                denom = denom + Pji * Pj;
            end
        end %j
        
        %denominate the prior for j (Pji*Pj(SUM_j(Pji*Pj)))
        p_i = p_i/denom;
        
        P_i = reshape(p_i, size(xx));
        Mx = p_i* dx;%
       [MxSort, ind_sort] = sort(Mx(:), 'descend');%ascend
        
        precise = (sum(MxSort)-sumMxSort_old)<pvalue/2;
        sumMxSort_old = sum(MxSort);
        n = n*1.5;
    end
    
    n;
    sum(MxSort);
    sz = numel(MxSort);
    s = 0;
    j = 0;
    %     conf = 1-pvalue;
    conf = sum(MxSort)*(1-pvalue);
    while(s <= conf && j<sz)
        j = j+1;
        s = s + MxSort(j) ;
    end
    
    if (j>=sz && pvalue ~= 0)
        classNum;
        errordlg('wrong pvalue or smth');
    end
    
    thr = MxSort(j)/(dx);
    
    if nargout>1
        %  output log

        T.xx = xx;

        mesh_cdf_ = cumsum(MxSort);
        mesh_cdf(ind_sort) = mesh_cdf_;

        T.Mx = reshape(mesh_cdf, size(xx));
        
        T.pdfSort = MxSort/(dx);
        T.MxSort = MxSort;

        BW = zeros(size(P_i));
        ind = find(P_i(:) >=thr);
        BW(ind) = 1;
        T.BW = BW;
    end
    
end
end




% % function [thr, T] = cdfMixture(feat, vdata, patchWeight, pvalue, classNum)
% % % thr : threshold for p(x|omega_i)
% % % T is a struct, logging data for the p(x|omega_i):
% % %    T.BW : binary mask for the computed threshold thr
% % %    T.xx, T.yy, T.zz : axis that were user for cdf computation
% %
% % level = size(vdata,2); % only for one level
% % subNums = size(vdata{level}, 2);
% % % classNum = 3;
% % [dim, featNum] = size(feat);
% %
% %
% % %figure out the boundaries for the integration
% % conf =  max(0.9974, 1-pvalue/5);
% % dthr = sqrt(qchisq(conf,dim));
% %
% % maxX = -inf;
% % maxY = -inf;
% % maxZ = -inf;
% % minX = inf;
% % minY = inf;
% % minZ = inf;
% %
% % for subNum = 1:subNums
% %     Mu = vdata{level}{subNum}.tle.Mu(:,classNum);
% %     Sigma = vdata{level}{subNum}.tle.Sigma(:,:,classNum);
% % % Mu = zeros([3,1]);
% % % Sigma = eye(3);
% %     %   supr = max(Mu)+50;
% %     %   infm = min(Mu)-50;
% %
% %     x0 = Mu(1);
% %     y0 = Mu(2);
% %     z0 = Mu(3);
% %
% %     Cxy = Sigma(1:2,1:2);
% %     Cyz = Sigma(2:3,2:3);
% %     Czx = Sigma([3 1],[3 1]);
% %
% %     [x,y,z] = getpoints(Cxy);
% %     maxX = max([maxX, max(x)*dthr+x0]);
% %     minX = min([minX, min(x)*dthr+x0]);
% %
% %     [y,z,x] = getpoints(Cyz);
% %     maxY = max([maxY, max(y)*dthr+y0]);
% %     minY = min([minY, min(y)*dthr+y0]);
% %
% %     [z,x,y] = getpoints(Czx);
% %     maxZ = max([maxZ, max(z)*dthr+z0]);
% %     minZ = min([minZ, min(z)*dthr+z0]);
% %
% % end
% %
% % % % compare to mahalanobis-distance-based threshold
% % % k = sqrt(qchisq(1-pvalue,dim));
% % % [eigvec,eigval] = eig(Sigma);
% % % A = [0,0,1]*sqrt(eigval)*eigvec';
% % % A(1) = (k*A(:,1)+x0);
% % % A(2) = (k*A(:,2)+y0);
% % % A(3) = (k*A(:,3)+z0);
% % % B = [0,1,0]*sqrt(eigval)*eigvec'
% % % B(1) = (k*B(:,1)+x0);
% % % B(2) = (k*B(:,2)+y0);
% % % B(3) = (k*B(:,3)+z0);
% % % C = [1,0,0]*sqrt(eigval)*eigvec';
% % % C(1) = (k*C(:,1)+x0);
% % % C(2) = (k*C(:,2)+y0);
% % % C(3) = (k*C(:,3)+z0);
% % % mvnpdf(A, Mu', Sigma)
% % % mvnpdf(B, Mu', Sigma)
% % % mvnpdf(C, Mu', Sigma)
% %
% % sumMxSort_old = 0;
% % n = 100;
% %
% % %  for subNum = 1:subNums
% % %
% % %  end
% %
% %
% % ind = 0;
% % for ii = 1:numel(patchWeight{2})
% %     mu = vdata{level}{ii}.tle.Mu;
% %     if ~isequal(mu, zeros(size(mu)))
% %         ind = ind+1;
% %         tPw{2}{ind} = patchWeight{2}{ii};
% %         tVdata{2}{ind}.tle.Mu = vdata{level}{ii}.tle.Mu;
% %         tVdata{2}{ind}.tle.Sigma = vdata{level}{ii}.tle.Sigma;
% %     end
% % end
% % patchWeight =  tPw;
% % vdata = tVdata;
% %
% % subNums = size(vdata{level}, 2);
% %
% % precise = false;
% % while ~precise
% %     % initialize the grid
% %
% %     % dx = max([abs(maxX-minX), abs(maxY-minY), abs(maxZ-minZ)])/n;
% %     dx = abs(maxX-minX)/n;
% %     dy = abs(maxY-minY)/n;
% %     dz = abs(maxZ-minZ)/n;
% %
% %     [xx,yy,zz] = meshgrid(minX:dx:maxX, minY:dy:maxY, minZ:dz:maxZ);
% %
% %     % re-normalize the patch weights (they overlap at box boundaries)
% %     sumPj = 0;
% %     for ii = 1:numel(patchWeight{2})
% %         sumPj = patchWeight{2}{ii} + sumPj;
% %     end
% %     for ii = 1:numel(patchWeight{2})
% %         patchWeight{2}{ii} = patchWeight{2}{ii}/sumPj;
% %     end
% %
% %     %integrate the sorted grid-sample
% %     p_i = zeros(size(xx(:),1),1);
% %     for subNum = 1:subNums
% %         Mu = vdata{level}{subNum}.tle.Mu(:,classNum);
% %         Sigma = vdata{level}{subNum}.tle.Sigma(:,:,classNum);
% %         %------------------------------------------------------------------
% % %         [u,s,v] = svd(Sigma);
% %        [u,s] = eig(Sigma);
% %         changed = false;
% %         if s(1,1)<dx
% %             s(1,1) = dx;
% %             changed = true;
% %         end
% %         if s(2,2)<dy
% %             s(2,2) = dy;
% %             changed = true;
% %         end
% %         if s(3,3)<dz
% %             s(3,3) = dz;
% %             changed = true;
% %         end
% %
% %         if changed
% %             Sigma = u*s*inv(u);
% %             %det(Sigma)
% %             %Sigma = ceil(Sigma)
% %             Sigma(2,1) = (Sigma(2,1)+Sigma(1,2))/2;
% %             Sigma(1,2) = Sigma(2,1);
% %             Sigma(3,1) = (Sigma(3,1)+Sigma(1,3))/2;
% %             Sigma(1,3) = Sigma(3,1);
% %             Sigma(3,2) = (Sigma(3,2)+Sigma(2,3))/2;
% %             Sigma(2,3) = Sigma(3,2);
% %             Sigma;
% %         end
% %
% % % %         dm = max([dx,dy,dz]);
% % % %         if s(1,1)<dx || s(2,2)<dy || s(3,3)<dz
% % % %             s(1,1) = dm;
% % % %             s(2,2) = dm;
% % % %             s(3,3) = dm;
% % % %             Sigma = s;%u*s*v';
% % % %         end
% %
% %         %------------------------------------------------------------------
% %         Pj = patchWeight{level}{subNum};
% %         p_i  = p_i +  Pj * mvnpdf([xx(:), yy(:), zz(:)],Mu', Sigma);%
% %
% %     end
% %
% %     P_i = reshape(p_i, size(xx));
% %     Mx = p_i* dx*dy*dz;%
% %     MxSort = sort(Mx(:), 'descend');%ascend
% %
% %     precise = (sum(MxSort)-sumMxSort_old)<pvalue/2;
% %     sumMxSort_old = sum(MxSort);
% %     n = n*1.5;
% % end
% %
% % n
% % sum(MxSort)
% %  sz = numel(MxSort);
% %  s = 0;
% %  j = 0;
% %  conf = 1-pvalue;
% %  while(s <= conf && j<sz)
% %      j = j+1;
% %      s = s + MxSort(j) ;
% % end
% %
% %  if (j>=sz)
% %      classNum
% %      errordlg('wrong pvalue or smth');
% %  end
% %
% %  thr = MxSort(j)/(dx*dy*dz)
% %
% %  if nargout>1
% %      %  output log
% %      T = struct;
% %      T.xx = xx;
% %      T.yy = yy;
% %      T.zz = zz;
% %
% %      %  create the binary mask for thresholded  p(x|omega_i)
% %      BW = zeros(size(P_i));
% %      ind = find(P_i(:) >=thr);
% %      BW(ind) = 1;
% %      T.BW = BW;
% %  end
% %
% % %  px = zeros(featNum,1);
% % %  for i = 1:featNum
% % %     px(i) = Mx(feat(1,i), feat(2,i), feat(3,i));
% % %  end
% %
% % end