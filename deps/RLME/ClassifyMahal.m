function [class, logg] = ClassifyMahal(feat, BW,  vdata, patchWeight, opts, bMahal,bdigital)
% Mahalanobis-distance based classification
if nargin<6
    bMahal = false;
end
if nargin<7
    bdigital = false;
end
[~, featNum] = size(feat);

if ~isfield(opts,'nclust')
    classNum = 3;
else
    classNum =opts.nclust;
end

pvalue = opts.pmaha;

MS = 10;

feat_cdf = zeros(featNum, classNum);

if ~bMahal 
   % calculate the significance-levels by numerical integration 
    for lab = 1:classNum
        [~, T{lab}] = cdfMixture(feat, vdata, patchWeight, min(pvalue,0.01), lab, classNum,bdigital);
    end

    for lab = 1:classNum
        % find the correspondence between feature pdfs and mesh pdfs
        ind{lab} = FindInd(T{lab}.feat_pdf(:,lab), T{lab}.pdfSort);
        %   ind{lab} = FindInd3(feat, [T{lab}.xx(:),T{lab}.yy(:), T{lab}.zz(:)]);
        %     mesh_cdf = T{lab}.Mx;
        %for ii = 1:size(T{lab}.Mx,3)
            % mesh_cdf(:,:,ii) = T{lab}.Mx(:,:,ii)';
            mesh_cdf = cumsum(T{lab}.MxSort);
        %end
        feat_cdf(:,lab) = mesh_cdf(ind{lab});
        
%         % the closest point in the mesh within the "core" of the distribution (confidence level <pvalue)
%         icore = find(T{lab}.MxSort<pvalue);
%         ind_core = FindInd(T{lab}.feat_pdf(:,lab), T{lab}.pdfSort(icore));
%         feat_core{lab}= [T{lab}.xx(icore(ind_core)),...
%             T{lab}.yy(icore(ind_core)),T{lab}.zz(icore(ind_core))];
        
%         % tmp: find for each feature the closest node in the mesh
%         idx = knnsearch([T{lab}.xx(:), T{lab}.yy(:)], feat');
%         % 
%         tfeat_cdf(:,lab) = mesh_cdf
    end
    
    % scale the confidence levels
%     for lab = 1:classNum
%         feat_cdf(:,lab) = (feat_cdf(:,lab)-min(feat_cdf(:,lab)))/(max(feat_cdf(:,lab))-min(feat_cdf(:,lab)));
%     end
    [min_feats, labels] = min(feat_cdf,[],2);
    logg.class0 = zeros(size(BW));
    logg.class0(BW>0) = labels;
    
    class = zeros(size(BW));
    labels(min_feats>1-pvalue) = MS;
    class(BW>0) = labels;
    
    logg.feat_cdf = feat_cdf;
    logg.min_feats = min_feats;
%     logg.feat_core = feat_core;
else
    % calculate the significance-levels by means of Mahalanobis distance
   
    % set dist threshold based on chi2 statistic
    dthr = 0:0.1:50;
    pval = 1-chi2cdf(dthr,size(feat,1));
    [mx,mi] = min(abs(pval-pvalue));
    dthr = dthr(mi);
    
    % calculate the mahalanobis distance, (only for global model)
    Mu = vdata{2}{1}.tle.Mu;
    Sigma = vdata{2}{1}.tle.Sigma;
    
    for i = 1:featNum
        y = feat(:,i);
        for lab = 1:classNum
            invSigma = inv(Sigma(:,:,lab));
            mu = Mu(:,lab);
            mdist(lab) = (y-mu)'*invSigma*(y-mu);
            feat_cdf(i,lab) = chi2cdf(mdist(lab),size(feat,1));
        end
        [mdist_min, lab_y] = min(mdist);
        if mdist_min > dthr
            labels(i) = MS;
        else
            labels(i) = lab_y;
        end
        logg.class0(i) = lab_y;
    end
    
    class = zeros(size(BW));
    class(BW>0) = labels;
    
    logg.feat_cdf = feat_cdf;
    %
    
end
% [~, labels] = min(feat_cdf,[],2);

% %% merge the probabilities onto a common mesh
% min_x = min([T{1}.xx(:); T{2}.xx(:);T{3}.xx(:)]);
% min_y = min([T{1}.yy(:); T{2}.yy(:);T{3}.yy(:)]);
% min_z = min([T{1}.zz(:); T{2}.zz(:);T{3}.zz(:)]);
%
% max_x = max([T{1}.xx(:); T{2}.xx(:);T{3}.xx(:)]);
% max_y = max([T{1}.yy(:); T{2}.yy(:);T{3}.yy(:)]);
% max_z = max([T{1}.zz(:); T{2}.zz(:);T{3}.zz(:)]);
%
% min_x = floor(min_x);
% min_y = floor(min_y);
% min_z = floor(min_z);
% max_x = ceil(max_x);
% max_y = ceil(max_y);
% max_z = ceil(max_z);
% [x,y,z] = meshgrid(min_x:max_x, min_y:max_y, min_z:max_z);

%
% %  [x,y,z] = meshgrid(1:255,1:255,1:255);
% % %[x,y,z] = meshgrid(1:4095,1:4095,1:4095);
% % feat_cdf = zeros([featNum,3]);
% % for lab = 1:classNum
% %     tt = interp3(T{lab}.xx,T{lab}.yy,T{lab}.zz, T{lab}.Mx, x,y,z, 'linear',1);
% %     %
% %     for ifeat = 1:featNum
% %         feat_cdf(ifeat,lab) = tt(feat(2,ifeat),feat(1,ifeat),feat(3,ifeat));
% %     end
% % end

% % %% set dist threshold based on chi2 statistic
% % dthr = 0:0.1:50;
% % pval = 1-chi2cdf(dthr,size(feat,1));
% % [mx,mi] = min(abs(pval-pvalue));
% % dthr = dthr(mi);
% % 
% % % calculate the mahalanobis distance, (only for global model)
% % featNum = size(feat,2);
% % Mu = vdata{2}{1}.tle.Mu;
% % Sigma = vdata{2}{1}.tle.Sigma;
% % 
% % for i = 1:featNum
% %     y = feat(:,i);
% %     for lab = 1:classNum
% %         invSigma = inv(Sigma(:,:,lab));
% %         mu = Mu(:,lab);
% %         mdist(lab) = (y-mu)'*invSigma*(y-mu);
% %     end
% %     [mdist_min, lab_y] = min(mdist);
% %     if mdist_min > dthr
% %         labels(i) = MS;
% %     else
% %         labels(i) = lab_y;
% %     end
% % end

end

function ind = FindInd3(feat3, mesh_int)
% find the correspondence between feature and mesh intensities
featNum = size(feat3,1);
[feat3_, ind_sort] = sort(feat3,'descend');
ind = zeros([featNum,1]);
imesh = 1;
nmesh = numel(pdfSort);
for ifeat = 1:featNum
    found = false;
    while ~found && imesh<nmesh
        found = feat1(ifeat) < pdfSort(imesh);
        imesh = imesh + 1;
    end
    ind(ifeat) = min(imesh, nmesh);
    imesh = imesh + 1;
end
ind = ind(ind_sort);
end

function ind = FindInd(feat1, pdfSort)
% find the correspondence between feature pdfs and mesh pdfs
featNum = size(feat1,1);
[feat1, ind_sort] = sort(feat1,'descend');
ind = zeros([featNum,1]);
imesh = 0;
nmesh = numel(pdfSort);
for ifeat = 1:featNum
    found = false;
    while ~found && imesh<nmesh
        imesh = imesh + 1;
        found = feat1(ifeat) >= pdfSort(imesh);
    end
    ind(ifeat) = min(imesh, nmesh);
    imesh = imesh - 1;
end
ind(ind_sort) = ind;
end








