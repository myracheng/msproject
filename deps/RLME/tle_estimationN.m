function [vdata, opts] = tle_estimationN( feat, seg, opts )

% check input args
if nargin<3, opts = struct; end

opts.bfast = 1;

% fill missing parameters with default values
if ~isfield(opts,'frac'), opts.frac = 0.05; end % fraction of outliers 
if ~isfield(opts,'nclust'), opts.nclust = 3; end % # of NABT clusters 
if ~isfield(opts,'maxiter_tle'), opts.maxiter_tle = 10; end % max # of tle iterations
if ~isfield(opts,'maxiter_gmm'), opts.maxiter_gmm = 50; end % max # of gmm iterations
if ~isfield(opts,'init_method'), opts.init_method = 'hierarchical'; end %kmeans , hierarchical
if ~isfield(opts,'verbose'), opts.verbose = 0; end % 0-disabled, 1-enabled
if ~isfield(opts,'sort_method'), opts.sort_method = 'loglik'; end % loglik, outlier belief
if ~isfield(opts,'exclude_method'), opts.exclude_method = 'delete'; end % interchange, delete

if isfield(opts,'imethod')
imethod = opts.imethod;
switch imethod
    case 2
        % proposed
        opts.sort_method = 'outlier_belief';
        opts.exclude_method = 'delete_maximizing';     
    case 1
        % fast-tle
        opts.sort_method = 'loglik';
        opts.exclude_method = 'delete';
end
end

if ~isfield(opts,'init'), opts.init = []; end % initialization for EM

if ~isfield(opts,'bMahal') ,opts.bMahal = false; end

vdata.opts = opts;
% map entries from the struct to vars
frac = opts.frac;
nclust = opts.nclust;
vdata.nclust = nclust;
maxiter_tle = opts.maxiter_tle;
maxiter_gmm = opts.maxiter_gmm;

init = opts.init;

sort_method = opts.sort_method;
exclude_method = opts.exclude_method;

% read params
featNum = size(feat, 2);
h = round(frac * featNum);
% init the set of points
i_pts = 1 : featNum;


if opts.verbose>0
    fprintf('initialize with given parameters...')
end
Mu0 = init.Mu;
Sigma0 = init.Sigma;
Priors0 = init.Priors;

Mu = Mu0;
Sigma = Sigma0;
Priors = Priors0;

loglik = realmin;

time2 = clock;
if opts.verbose>0
    fprintf('running TLE procedure...')
end

loglik_threshold = 1e-10;%eps
% % maxiter_tle = 10000;%almost until convergence
loglik_old = -realmax;
nbStep = 0;

Mu0 = Mu;
Sigma0 = Sigma;
Priors0 = Priors;

priors = Priors;
mu = Mu;
sigma = Sigma;

L(1) = nan;
H(1) = featNum-h;
newTLs = nan;
if opts.frac>0
    % run tle loop
    for iter_tle = 1 : maxiter_tle
        % display output
        if opts.verbose>0
            fprintf('\n\ttle iteration #%d...',iter_tle)
        end
        time1 = clock;
        
        pdfs = zeros(numel(i_pts),nclust);%probability density
        conf = zeros(numel(i_pts),nclust); %confidence
        for iter_clust = 1 : nclust
            
            if ~isfield(opts, 'bfast')
                pdfs(:,iter_clust) = mvnpdf(feat(:,i_pts)',Mu(:,iter_clust)',Sigma(:,:,iter_clust));
            else
                [pdfs(:,iter_clust), conf(:,iter_clust)] = mvnpdfN(feat(:,i_pts)',Mu(:,iter_clust)',Sigma(:,:,iter_clust));
            end
            
        end
        
        MRFprior = repmat(Priors, [featNum, 1]);
        
        % maximum a posteriori classification
        Pix = MRFprior.*pdfs;
        Pix = Pix./repmat(sum(Pix,2),1,nclust);
        [~, max_prob_ind] = max(Pix, [], 2);
        
        % select subset of points based on probability rule
        nsel = featNum - h;
        if strcmp(sort_method, 'loglik')
            if opts.verbose>0
                fprintf('\nsort according to log-likelihood...')
            end
            % sort the likelihoods
            probTheta = sum(MRFprior.*pdfs, 2);%
            %    [prob_sort, iix] = sort(probTheta,'descend');%
            [prob_sort, iix] = sort(log(probTheta),'descend');%
            
        elseif strcmp(sort_method, 'outlier_belief')
            if opts.verbose>0
                fprintf('\nsort according to outlier belief...')
            end
            if ~isfield(opts,'bfast')
                vdata1{2}{1}.tle.Mu = Mu;
                vdata1{2}{1}.tle.Sigma = Sigma;
                vdata1{2}{1}.tle.Priors = Priors;
                patchWeight1{2}{1} = 1;
                opts1.pmaha = 0.01;%
                opts1.nclust = nclust;
                % compute the confidence levels from each class
                [ClassM, logg] = ClassifyMahal(feat, seg,  vdata1, patchWeight1, opts1, opts.bMahal);
                conf = logg.feat_cdf;
            end
            probTheta = zeros([featNum,1]);
            
            % choose the classifier
            if isfield(opts,'classifier') && strcmp(opts.classifier,'mahalanobis')
                if ~isfield(opts,'bfast')
                    classif = logg.class0(i_pts);%ix
                else
                    [~,classif] = max(conf,[],2);
                end
            else
                classif = max_prob_ind;
            end
            
            % compute the confidence levels from the class the points belong to according the Bayes rule
            for i = 1:featNum
                probTheta(i) = conf(i,classif(i));
            end
            [prob_sort, iix] = sort(probTheta,'ascend');
        end
        
        if strcmp(exclude_method, 'delete_maximizing')
            if opts.verbose>0
                fprintf('\ndelete the outliers while maximizing loglik...')
            end
            %%
            %             Lik = sum(MRFprior(iix, :).*prob(iix, :), 2);
            % add a small value for numerical stability
            Lik = sum(MRFprior(iix, :).*(pdfs(iix, :)+eps), 2);
            % lik(lik>1) = 1; %exclude positive logliks
            Logliks = log(Lik);
            newTL = cumsum(Logliks);
            [lik_sort, iix_lik] = sort(log(sum(MRFprior.*pdfs, 2)),'descend');%
            cum_lik_sort = cumsum(lik_sort);
            maxTL = cum_lik_sort(nsel);
            maxL(iter_tle) = maxTL;
            
            if newTL(nsel)<loglik_old
                %if negTL(nsel-numel(find(indPositives)))<loglik_old
                %sub_i_pts = i_pts([iix(indPositives); iix(~indPositives(1:negmax))]);
                %H(iter_tle) = numel(sub_i_pts);
                
                % im = index maximal - keep as much inliers as TL allows
                % iix is ordered from inliers to outliers (descending order by significance level)
                % cumulative log-likelihood is increasing on positive
                % logliks (thus when corresponding pdfs are >1) and is
                % decreasing on negative logliks (when pdfs<1 that is
                % common for outliers, heavy tails and scattered
                % distributions in general
                im = max(find(newTL>loglik_old));
                maxTL>newTL(im);
                if isempty(im),
                    break;
                end
                if maxTL<newTL(im)
                    break;
                end
                newTLs(iter_tle) = newTL(im);
                H(iter_tle) = im;
                sub_i_pts = i_pts(iix(1:H(iter_tle)));
                
            else
                newTLs(iter_tle) = newTL(nsel);
                H(iter_tle) = nsel;
                sub_i_pts = iix(1:nsel);
            end
            %%
            % run em-gmm on a subset of points
            [Priors, Mu, Sigma, ~, loglik] = EM(feat(:,sub_i_pts),...
                Priors, Mu, Sigma, maxiter_gmm);%
            
        elseif strcmp(exclude_method, 'delete')
            if opts.verbose>0
                fprintf('\ndelete the outliers...')
            end
            % select first nsel samples
            sub_i_pts = i_pts(iix(1:nsel));
            
            % run em-gmm on a subset of points
            [Priors, Mu, Sigma, ~, loglik] = EM(feat(:,sub_i_pts),...
                Priors, Mu, Sigma, maxiter_gmm);
        end
        
        if (1-(loglik/loglik_old)) < loglik_threshold
            nbStep;
            for iter_clust = 1:nclust
                if det(Sigma(:,:,iter_clust))<0
                    Sigma(:,:,iter_clust) = Sigma(:,:,iter_clust)+eye(3)*1e-03;
                end
            end
            break;
        end
        loglik_old = loglik;
        
        L(iter_tle) = loglik;
        
        nbStep = nbStep+1;
        
        if opts.verbose>0
            fprintf('time: %1.1f',etime(clock,time1))
        end
    end
    
else % frac==0
    % run EM on the whole set of samples
    [Priors, Mu, Sigma, ~, loglik] = EM(feat,... %feat_t1_sub
        Priors0, Mu0, Sigma0, maxiter_gmm);
    
    % record process time
    tle_process_time = etime(clock,time2);
    if opts.verbose>0
        if opts.verbose>0
            fprintf('\n total')
        end
        fprintf('time: %1.1f s\n',tle_process_time)
    end
    %     vdata = struct;
    vdata.prob_sort = [];
    vdata.iix = [];
    vdata.L = [];
    if strcmp(exclude_method, 'delete_maximizing')
        vdata.H = [];
        vdata.maxL = [];
        vdata.newTL = [];
    end
    vdata.tle.Mu = Mu;
    vdata.tle.Sigma = Sigma;
    vdata.tle.Priors = Priors;
    % store classification params
    vdata.class.dthr = [];
    %store the final trimmed sample
    vdata.sub_i_pts = [];
    vdata.conf = [];%conf;
    % vdata.class.pvalue = 1-chi2cdf(dist,nclust);
    % vdata.featRec = featRec;
    
    % save initially estimated params
    vdata.Priors0 = Priors0;
    vdata.Mu0 = Mu0;
    vdata.Sigma0 = Sigma0;
    
    % save the classification
    for iter_clust = 1:nclust
        if size(feat,1)>1
            pdfs(:,iter_clust) = mvnpdf(feat(:,i_pts)',Mu(:,iter_clust)',Sigma(:,:,iter_clust));
        else
            pdfs(:,iter_clust) = pdf('norm',feat(:,i_pts),Mu(:,iter_clust),sqrt(Sigma(:,:,iter_clust)));
            
        end
    end
    
    MRFprior = repmat(Priors, [featNum, 1]);
    
    [~, max_prob_ind] = max(MRFprior.*pdfs, [], 2);
    vdata.max_prob_ind = max_prob_ind;
    vdata.pxi = pdfs;
    vdata.feat_box = [min(feat,[],2),max(feat,[],2)];
    return
end

% record process time
tle_process_time = etime(clock,time2);
if opts.verbose>0
    if opts.verbose>0
        fprintf('\n total')
    end
    fprintf('time: %1.1f s\n',tle_process_time)
end

vdata.prob_sort = prob_sort;
vdata.iix = iix;
% store TLE data
vdata.L = L;
if strcmp(exclude_method, 'delete_maximizing')
    if iter_tle>0
        vdata.H = H;
        vdata.maxL = maxL;
        vdata.newTL = newTLs;
    end
end
vdata.tle.Mu = Mu;
vdata.tle.Sigma = Sigma;
vdata.tle.Priors = Priors;

%store the final trimmed sample
vdata.sub_i_pts = sub_i_pts;
vdata.conf = probTheta;%conf;

% save initial params
vdata.Priors0 = Priors0;
vdata.Mu0 = Mu0;
vdata.Sigma0 = Sigma0;

%%
[~, max_prob_ind] = max(MRFprior.*pdfs, [], 2);
vdata.max_prob_ind = max_prob_ind;
vdata.pxi = pdfs;
vdata.feat_box = [min(feat,[],2),max(feat,[],2)];
end
