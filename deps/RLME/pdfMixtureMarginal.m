
function [pxOmega,priorsOmega] = pdfMixtureMarginal(feat, vdata, patchWeight, modality)

level = size(vdata,2); % only for one level
subNums = size(vdata{level}, 2);

ClassNum = 3;

denom = zeros([1,ClassNum]);
for i = 1:ClassNum
    denom(i) = 0;
    for subNum = 1:subNums
        PiPatch = patchWeight{level}{subNum};
        Pji = vdata{level}{subNum}.tle.Priors(:,i);
        if ~isnan(Pji)
            denom(i) = denom(i) + Pji * PiPatch;
        end
    end
end

pxOmega = zeros(size(feat,2), ClassNum);
for lab = 1:ClassNum    
    for subNum = 1:subNums
        Mu = vdata{level}{subNum}.tle.Mu;
        Sigma = vdata{level}{subNum}.tle.Sigma;
        PiPatch = patchWeight{level}{subNum};
        Pji = vdata{level}{subNum}.tle.Priors(:,lab);
        if ~isnan(Pji)&&Pji>0
            pxOmega(:,lab) = pxOmega(:,lab) + Pji*PiPatch * pdf('norm',feat',Mu(modality,lab),sqrt(Sigma(modality,modality,lab)));
        end
    end
    pxOmega(:,lab) = pxOmega(:,lab)/denom(lab);
end
priorsOmega = denom;

end