function plot_ellipse2(Mu,Sigma, color,lw, lstyle)
if nargin<5
    lstyle = '-';
end
if nargin<4
    lw = 1;
end
% number of vertices drawn for each frame
np        = 40 ;
tmp = sqrt(Sigma(2,2)) + eps ;
A(1,:) = sqrt(Sigma(1,1)*Sigma(2,2) - Sigma(1,2)^2) / tmp ;
A(2,:) = 0;
A(3,:) = Sigma(1,2) / tmp ;
A(4,:) = tmp ;
AA = reshape(A,2,2);
thr = linspace(0,2*pi,np) ;
Xp = [cos(thr) ; sin(thr) ] ;
dthr = sqrt(qchisq(0.9973,2)) ;
X = dthr*AA * Xp ;
X(1,:) = X(1,:) + Mu(1) ;
X(2,:) = X(2,:) + Mu(2) ;
line(X(1,:), X(2,:),'Color',color,'LineWidth',lw, 'Linestyle', lstyle)