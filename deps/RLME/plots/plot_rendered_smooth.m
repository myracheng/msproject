function plot_rendered_smooth(BW, color, alpha)
if nargin<3
    alpha=0.3;
end
if nargin<2
    color = 'red';
end
[x,y,z] = meshgrid(1:size(BW,2), 1:size(BW,1),1:size(BW,3));

bw = imfilter(double(imdilate(BW,ones(7,7,7))),fspecial('disk',3));
for ii = 1:size(bw,1)
    bw(ii,:,:) = imfilter(double(squeeze(bw(ii,:,:))),fspecial('disk',3));
end
for ii = 1:size(bw,2)
    bw(:,ii,:) = imfilter(double(squeeze(bw(:,ii,:))),fspecial('disk',3));
end
bw = imerode(bw,ones(5,5,5));
fv = isosurface(x, y, z, bw, 0.7);
p = patch(fv);
set(p,'FaceColor',color,'EdgeColor','none','FaceAlpha',alpha,'EdgeAlpha',0.0);
daspect([1 1 1])
view(3); 
camlight
% lighting phong
lighting gouraud