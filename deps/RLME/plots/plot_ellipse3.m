function plot_ellipse3(mu,sigma, color,lw,projections)

%Mu, Sigma are 3D: of size [1:3]' and (1:3,1:3), 

if nargin<4
    lw = 1;
end
if nargin<5
    projections = 1:3;
end
% number of vertices drawn for each frame
np        = 40 ;

xy = 1:2;
xz = [1,3];
yz = 2:3;

ax = [xy; xz; yz];

for i = projections%1:3
    ind = ax(i,:);
    Mu = mu(ind);
    Sigma = sigma(ind,ind);
    tmp = sqrt(Sigma(2,2)) + eps ;
    A(1,:) = sqrt(Sigma(1,1)*Sigma(2,2) - Sigma(1,2)^2) / tmp ;
    A(2,:) = 0;
    A(3,:) = Sigma(1,2) / tmp ;
    A(4,:) = tmp ;
    AA = reshape(A,2,2);
    thr = linspace(0,2*pi,np) ;
    Xp = [cos(thr) ; sin(thr) ;] ;
    X = 3*AA * Xp ;
    X(1,:) = X(1,:) + Mu(1) ;
    X(2,:) = X(2,:) + Mu(2) ;
    
    Z = zeros(size(X(1,:))); %dummy axis
    
    if i == 1
        line(X(1,:), X(2,:),Z(1,:),'Color',color,'LineWidth',lw); %xy
    elseif i == 2
        line(X(1,:),Z(1,:), X(2,:),'Color',color,'LineWidth',lw); %xz
    else
        line(Z(1,:), X(1,:), X(2,:),'Color',color,'LineWidth',lw); %yz
    end
end