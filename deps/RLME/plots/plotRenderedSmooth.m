
function plotRenderedSmooth(x, y, z, BW, color, alpha)
%%
if nargin<6
    alpha=0.3;
end
if nargin<5
    color = 'red';
end
% clf
% 
% [ix,iy,iz] = meshgrid(min(x(:)):(max(x(:))-min(x(:)))/size(x,1)/5:max(x(:)),...
%     min(y(:)):(max(y(:))-min(y(:)))/size(y,1)/5:max(y(:)),...
%     min(z(:)):(max(z(:))-min(z(:)))/size(z,1)/5:max(z(:)));
% 
% iBW = interp3(x,y,z,BW,ix,iy,iz);
% 
% fv = isosurface(ix, iy, iz, iBW, 0.9);

bw = imfilter(double(imdilate(BW,ones(7,7,7))),fspecial('disk',3));
for ii = 1:size(bw,1)
    bw(ii,:,:) = imfilter(double(squeeze(bw(ii,:,:))),fspecial('disk',3));
end
for ii = 1:size(bw,2)
    bw(:,ii,:) = imfilter(double(squeeze(bw(:,ii,:))),fspecial('disk',3));
end
bw = imerode(bw,ones(5,5,5));
fv = isosurface(x, y, z, bw, 0.7);
p = patch(fv);
% isonormals(BW,p)
set(p,'FaceColor',color,'EdgeColor','none','FaceAlpha',alpha,'EdgeAlpha',0.3);
daspect([1 1 1])
view(3); 
camlight 
lighting gouraud