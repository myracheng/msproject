function plotRendered(x, y, z, BW, color, alpha)
%%
if nargin<6
    alpha=0.3;
end
if nargin<5
    color = 'red';
end
% clf
% 
% [ix,iy,iz] = meshgrid(min(x(:)):(max(x(:))-min(x(:)))/size(x,1)/5:max(x(:)),...
%     min(y(:)):(max(y(:))-min(y(:)))/size(y,1)/5:max(y(:)),...
%     min(z(:)):(max(z(:))-min(z(:)))/size(z,1)/5:max(z(:)));
% 
% iBW = interp3(x,y,z,BW,ix,iy,iz);
% 
% fv = isosurface(ix, iy, iz, iBW, 0.9);

fv = isosurface(x, y, z, BW, 0.9);
p = patch(fv);
% isonormals(BW,p)
set(p,'FaceColor',color,'EdgeColor','none','FaceAlpha',alpha,'EdgeAlpha',0.3);
daspect([1 1 1])
view(3); 
camlight 
lighting gouraud