%%
addpath(genpath(cd))
%% mixture parameters
ClassNum = 3; %2,3,4,5
dim = 2; %1,2,3
theta = struct('mu', zeros(dim,ClassNum), 'sigma', zeros(dim,dim,ClassNum), 'priors', zeros(dim,1));
frac = 0;
SampleSize = 1e5;
%% set dist threshold based on chi2 statistic
pmaha = 1-0.95; % as in Fritz
dthr = 0:0.1:50;
pval = 1-chi2cdf(dthr,dim);
[mx,mi] = min(abs(pval-pmaha));
dthr = dthr(mi);
%% define model params explicitly
Mu = [0 2; 2 0; -2 0]';
Sigma(:,:,1) = [2 0.5; 0.5 0.5];
Sigma(:,:,2) = [0.1 0; 0 0.1];
Sigma(:,:,3) = [2 -0.5; -0.5 0.5];

% Sigma(:,:,1) = [2 0; 0 0.5];
% Sigma(:,:,2) = [0.1 0; 0 0.1];
% Sigma(:,:,3) = [2 -0; -0 0.5];

Priors = [ 0.1 0.5 0.4];
% calculate the complexity of the mixture
theta = struct('Mu', Mu, 'Sigma', Sigma, 'Priors', Priors);
theta = struct('Mu', Mu, 'Sigma', Sigma, 'Priors', Priors);
[p_post, dOmega] = PosteriorMesh(theta,'gmm');
optsBER.dx = dOmega(1); 
if dim>1
    optsBER.dy = dOmega(2);
end
if dim>2
    optsBER.dz = dOmega(3);
end
BER = BayesError(p_post, optsBER)

[Data,GT] = GenerateGMM(SampleSize, theta.Mu, theta.Sigma, theta.Priors);

clf
plot(Data(:,1), Data(:,2), '.', 'MarkerSize', 3, 'color', [0.5 0.5 0.5])

cols = colormap(jet(ClassNum+1));
cols(1,:) = [];
for lab = 1:ClassNum
    plot_ellipse2(theta.Mu(:,lab), theta.Sigma(:,:,lab), cols(lab,:));
end
set(gca, 'XLim', [min(Data(:)),max(Data(:))])
set(gca, 'YLim', [min(Data(:)),max(Data(:))])
set(gca, 'XTick', round([min(Data(:,1)),0,max(Data(:,1))]))
set(gca, 'YTick', round([min(Data(:,2)),0,max(Data(:,2))]))
box on
axis square

%%
set(0,'DefaultFigureWindowStyle','docked');
figure
verbose = 1;
methods = 1:2; % 1: fast-tle, 2: proposed
init_method = 'kmeans';%'kmeans', 'otsu','GT','gtclose'
Hs = [0.0:0.05:0.5];
if verbose
    cols = colormap(jet(numel(Hs)+1));
    cols(1,:) = [];
end
MCR_ = [];
%
opts = struct;
opts.verbose = verbose;
opts.maxiter_tle = 50;
if strcmp(init_method,'kmeans')
    %opts.init_method = 'kmeans';
    % compute initial classification by kmeans
    [Class0, C] = kmeans(Data,ClassNum,'start', 'cluster');%, 'start', Mu'
    ClassInit = zeros(size(Class0));
    [~, lxi] = sort(C(:,1), 'ascend');
    for lab = 1:ClassNum
        ClassInit(Class0==lxi(lab)) = lab;
    end
    opts.init.Mu = C';
    for lab = 1:ClassNum
        opts.init.Sigma(:,:,lab) = eye(dim)*0.3;
        opts.init.Priors(lab) = nnz(ClassInit==lab)/nnz(ClassInit);
    end
elseif strcmp(init_method,'otsu')
    % compute initial classification by otsu thresholding on first coordinate
    ClassInit = otsu(Data(:,1),ClassNum-1)+1;
    for lab = 1:ClassNum
        opts.init.Sigma(:,:,lab) = eye(dim)*0.3;
        opts.init.Priors(lab) = nnz(ClassInit==lab)/nnz(ClassInit);
        opts.init.Mu(:,lab) = mean(Data(ClassInit==lab,:));
    end
elseif strcmp(init_method,'GT')
    % init by GT parameter values
    opts.init.Mu = theta.Mu;
    opts.init.Sigma = theta.Sigma;
    opts.init.Priors = theta.Priors;
elseif strcmp(init_method, 'gtclose')
    opts.init.Priors = repmat(1/3,[1,ClassNum]);
    for lab = 1:ClassNum
        opts.init.Sigma(:,:,lab) = eye(dim)*0.3;
        % randomly select the means from inlier region (95%)
        b_inliers = zeros(size(Data));
        for iy = find(GT==lab)'
            y = Data(iy,:);
            diff = y'-theta.Mu(:,lab);
            invsigma = inv(theta.Sigma(:,:,lab));
            smd_ = diff'*invsigma*diff;
            if smd_<dthr
                b_inliers(iy) = 1;
            end
        end
        
        inds = find(b_inliers);
        opts.init.Mu(:,lab) = Data(randsample(inds,1),:)';
    end
    
end

% run the robust estimators
for imethod = methods
    subplot(1,numel(methods),imethod)
    plot(Data(:,1), Data(:,2), '.', 'MarkerSize', 3, 'color', [0.5 0.5 0.5])
    set(gca, 'XLim', [min(Data(:)),max(Data(:))])
    set(gca, 'YLim', [min(Data(:)),max(Data(:))])
    set(gca, 'XTick', round([min(Data(:,1)),0,max(Data(:,1))]))
    set(gca, 'YTick', round([min(Data(:,2)),0,max(Data(:,2))]))
    box on
    axis square
    kk = 0;
    for h = Hs
        kk = kk+1;
        opts.frac = h;
        opts.imethod = imethod;
        Vdata = RMLE_GMM( Data', opts);
        Class = Vdata.classif;
        
        if verbose
            mu = Vdata.tle.Mu;%Vdata.mu0;
            sigma = Vdata.tle.Sigma;%Vdata.sigma0;
            priors = Vdata.tle.Priors;%Vdata.priors0;
            
            color = cols(kk,:);
            lw = 1.2;
            hold on
            plot_ellipse2(mu(1:2,1), sigma(1:2,1:2,1), color, lw)
            plot_ellipse2(mu(1:2,2), sigma(1:2,1:2,2), color, lw)
            plot_ellipse2(mu(1:2,3), sigma(1:2,1:2,3), color, lw)

        end
        % compute the misclassification ratio 
        MCR_(imethod,kk) = MCR(Class,GT);%assuming the labels are not switched
    end
    if verbose
        % show the initial estimates
        theta_ = Vdata.opts.init;
        for lab = 1:ClassNum
            plot_ellipse2(theta_.Mu(:,lab),theta_.Sigma(:,:,lab),'k',1.5,'-')
        end
        % show the ground truth parameters
        theta_ = theta;
        for lab = 1:ClassNum
            plot_ellipse2(theta_.Mu(:,lab),theta_.Sigma(:,:,lab),'k', 1.5,'--')
        end
    end
end