function vdata = RMLE_GMM( feat, opts )
% Robust Maximum Likelihood Estimator for Gaussian Mixture Models
%

% if nargin<2, opts = struct; end

% fill missing parameters with default values
if ~isfield(opts,'frac'), opts.frac = 0.05; end % fraction of outliers
if ~isfield(opts,'nclust'), opts.nclust = 3; end % # of NABT clusters
if ~isfield(opts,'maxiter_tle'), opts.maxiter_tle = 10; end % max # of tle iterations
if opts.frac==0, opts.maxiter_tle = 1; end % switch to em-gmm
if ~isfield(opts,'maxiter_gmm'), opts.maxiter_gmm = 50; end % max # of em-gmm iterations
if ~isfield(opts,'imethod'), opts.init_method = 2; end 
if ~isfield(opts,'verbose'), opts.verbose = 0; end % 0-disabled, 1-enabled
if ~isfield(opts,'sort_method'), opts.sort_method = 'loglik'; end % loglik, outlier_belief
if ~isfield(opts,'exclude_method'), opts.exclude_method = 'delete'; end % delete_maximizing, delete

assert(isfield(opts,'init')) % initialization for em-gmm
assert(opts.frac<1 & opts.frac>=0) % fraction of outliers check

if isfield(opts,'imethod')
    imethod = opts.imethod;
    switch imethod
        case 2
            % proposed
            opts.sort_method = 'outlier_belief';
            opts.exclude_method = 'delete_maximizing';
        case 1
            % fast-tle
            opts.sort_method = 'loglik';
            opts.exclude_method = 'delete';
    end
end

% save the input parameters
vdata.opts = opts;

% map entries from the struct to vars
frac = opts.frac;
nclust = opts.nclust;
maxiter_tle = opts.maxiter_tle;

maxiter_gmm = opts.maxiter_gmm;
init = opts.init;
sort_method = opts.sort_method;
exclude_method = opts.exclude_method;

% read params
nfeat = size(feat, 2);
h = round(frac * nfeat);
% init the set of points
inds = 1 : nfeat;

if opts.verbose>0
    fprintf('initialize with given parameters...')
end
Mu = init.Mu;
Sigma = init.Sigma;
Priors = init.Priors;

time2 = clock;
if opts.verbose>0
    fprintf('running TLE procedure...')
end

loglik_threshold = 1e-10;
loglik_old = -realmax;
nbStep = 0;

% number of inliers 
nsel = nfeat - h;

L(1) = nan;
H(1) = nsel;
newTLs = nan;

% run tle loop
for iter = 1 : maxiter_tle
    % display output
    if opts.verbose
        fprintf('\n\titeration #%d...',iter)
    end
    time1 = clock;
    
    pdfs = zeros(nfeat,nclust); %likelihoods
    MDs = zeros(nfeat,nclust); %mahalanobis distances
    for iter_clust = 1 : nclust
        [pdfs(:,iter_clust), MDs(:,iter_clust)] = mvnpdfN(feat(:,inds)',Mu(:,iter_clust)',Sigma(:,:,iter_clust));
    end
    
    WPriors = repmat(Priors, [nfeat, 1]);
    
    % maximum a posteriori classification
    Pix = WPriors.*pdfs;
    Pix = Pix./repmat(sum(Pix,2),1,nclust);
    [~, classif] = max(Pix, [], 2);
    
    % ordering scheme for the outlier/inlier selection
    if strcmp(sort_method, 'loglik')
        if opts.verbose
            fprintf('\nsort according to log-likelihood...')
        end
        % sort the likelihoods
        ordering = sum(WPriors.*pdfs, 2);
        [order_sort, order_inds] = sort(log(ordering),'descend');
        
    elseif strcmp(sort_method, 'outlier_belief')
        if opts.verbose
            fprintf('\nsort according to outlier belief...')
        end

        % compute the confidence levels from the class the points belong to according the MAP rule
        ordering = zeros([nfeat,1]);
        for iclust = 1:nclust
            ordering(classif==iclust) = MDs(classif==iclust,iclust);
        end
        [order_sort, order_inds] = sort(ordering,'ascend');
    end
    
    % trim the outliers
    if nsel<nfeat
        if strcmp(exclude_method, 'delete')
            if opts.verbose>0
                fprintf('\ndelete the outliers by loglik...')
            end
            % select first nsel samples, i.e. the inliers
            inliers = inds(order_inds(1:nsel));
            
        elseif strcmp(exclude_method, 'delete_maximizing')
            if opts.verbose>0
                fprintf('\ndelete the outliers while maximizing loglik...')
            end
            %%
            % add a small value for numerical stability
            Lik = sum(WPriors(order_inds, :).*(pdfs(order_inds, :)+eps), 2);
            % Lik(Lik>1) = 1; %exclude positive logliks
            Logliks = log(Lik);
            newTL = cumsum(Logliks);
            lik_sort = sort(log(sum(WPriors.*pdfs, 2)),'descend');
            cum_lik_sort = cumsum(lik_sort);
            maxTL = cum_lik_sort(nsel);
            maxL(iter) = maxTL;
            
            if newTL(nsel)<loglik_old                
                % imax = index maximal - keep as much inliers as TL allows
                % iix is ordered from inliers to outliers (descending order by significance level)
                % cumulative log-likelihood is increasing on positive
                % logliks (thus when corresponding pdfs are >1) and is
                % decreasing on negative logliks (when pdfs<1 that is
                % common for outliers, heavy tails and scattered
                % distributions in general
                imax = max(find(newTL>loglik_old));
                if isempty(imax),
                    break;
                end
                if maxTL<newTL(imax)
                    break;
                end
                newTLs(iter) = newTL(imax);
                H(iter) = imax;
                inliers = inds(order_inds(1:H(iter)));
                
            else
                newTLs(iter) = newTL(nsel);
                H(iter) = nsel;
                inliers = order_inds(1:nsel);
            end
        end
    else
        % no trimming needed
        inliers = inds;
    end
    
    % run em-gmm on a [sub]set of points
    [Priors, Mu, Sigma, ~, loglik] = EM(feat(:,inliers),...
        Priors, Mu, Sigma, maxiter_gmm);
    
    if (1-(loglik/loglik_old)) < loglik_threshold
        for iter_clust = 1:nclust
            if det(Sigma(:,:,iter_clust))<0
                % address the singularities
                Sigma(:,:,iter_clust) = Sigma(:,:,iter_clust)+eye(3)*1e-03;
            end
        end
        break;
    end
    loglik_old = loglik;
    
    L(iter) = loglik;
    
    nbStep = nbStep+1;
    
    if opts.verbose>0
        fprintf('time: %1.1f',etime(clock,time1))
    end
end


% record process time
tle_process_time = etime(clock,time2);
if opts.verbose
    if opts.verbose>0
        fprintf('\n total')
    end
    fprintf('time: %1.1f s\n',tle_process_time)
end

vdata.order_sort = order_sort;
vdata.order_inds = order_inds;

% store TLE data
vdata.Mu = Mu;
vdata.Sigma = Sigma;
vdata.Priors = Priors;

% store the final trimmed sample
vdata.inliers = inliers;
vdata.ordering = ordering;

% store the final classification
[~, classif] = max(WPriors.*pdfs, [], 2);
vdata.classif = classif;
% vdata.pdfs = pdfs;
end
