%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% read 2D example
im = imread('./images/brain.png');
%% visualize the image
figure, imshow(im)
%% create the manual segmentaton
imagesc(im)
axis image
colormap gray
[x, y] = getline(gcf,'closed');
roi = struct('x',[],'y',[]); %save the region of interest(ROI) as a structure
roi.x = x;
roi.y = y;
%% overlay the segmentation
hold on, plot(roi.x,roi.y,'g')
%% load the segmentation
seg = poly2mask(roi.x,roi.y,size(im,1),size(im,2)); %save the region of interest(ROI) as a binary 2D matrix
imshow(seg)
%% save the segmentation, both roi and the mask
DB = {}; % create a database (DB) with our images and annotations
icase = 1; % we have only one case(patient) so far
DB{icase}.img = im;
DB{icase}.roi = roi;
DB{icase}.seg = seg;
save('./DB.mat','DB')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TODO: load and view the segmentation
% clear all % remove all the variables
load('./DB.mat','DB')

% TODO: get the image as img, get the segmentation as mask and regions of
% interest coordinates as x and y done

img = im;
mask = seg;
x = roi.x;
y = roi.y;

figure, % new image plot

subplot(1,2,1)
imagesc(img), % draw the original image
axis image, axis off, colormap gray % visualization options
hold on, plot(x,y,'g') % overlay the lines

subplot(1,2,2)
imagesc(img), % draw the original image
axis image, axis off, colormap gray % visualization options
hold on, contour(mask,1,'m') % plot the contours of segmentation

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TODO: read and view DICOM image and information from './images/brain.dcm'
% load the image
a = dicomread('./images/brain.dcm');
% load the information header
info = dicominfo('./images/brain.dcm');
% plot the image
% figure,
figure, imagesc(a)
axis image
colormap gray

% display information about the subject found in the header: Modality and
% PatientAge
disp(info.Modality);
disp(info.PatientAge);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TODO: load a 3D DICOM image slice-by-slice from './images/brain/*.dcm'
image_slice_files = dir('./images/brain/*.dcm'); % get all the needed file names (20)
V = []; % store image volume as a matrix V
% read and store the image information from the files here: one file at a time
% ... your code hereimagesc(im)
for loop = 1:length(image_slice_files)
    filename = fullfile(image_slice_files(loop).folder, image_slice_files(loop).name);
    a = dicomread(filename);
    V(:,:,loop)=a;
end
%% view the images in 3D
ViewerGUI(V)
%% save the volume in matlab format as a 3D matrix
if ~isdir('./images/output'),mkdir('./images/output'); end % create the directory where to store
save('./images/output/V.mat','V')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TODO: load another brain image volumes from './images/tcia-gbm/t2'
% note: here you will need to read some information from header too
image_slice_files = dir('./images/tcia-gbm/t2/*.dcm'); % get all the needed file names (20)
t2 = []; % store image volume as a matrix t2
% read and store the image information from the files here: one file at a time
%...
for loop = 1:length(image_slice_files)
    filename = fullfile(image_slice_files(loop).folder, image_slice_files(loop).name);
    a = dicomread(filename);
    b = dicominfo(filename);
    t2(:,:,b.InstanceNumber)=a;
end
%% view the images in 3D, make sure they look good on all projections
ViewerGUI(t2)
%% the volume you see should look like this:
im = imread('./images/t2_screenshot.PNG');
figure, imshow(im)
%% save the volume in matlab format as a 3D matrix
% TODO: create the directory where to store
% ... 
mkdir('./images/output/tcia')
save('./images/output/tcia/t2.mat','t2') 









