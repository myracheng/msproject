% save standardized nii and distances
%% init vars
normmethod = 'zscore'; % normalization method: 'zscore','wm'
patients = 1:20;%[1,2,4,5,6,7,8,10,12,14,15,16,17,18,19];

%% get file names
for J = 1:length(patients)
    patient_number = patients(J);
    num = num2str(patient_number);
    folder = strcat('../MSpatientdata/patient', num);
    % Get a list of all files in the folder with the desired file name pattern
    filePattern = fullfile(folder, '*.nii*'); % Change to whatever pattern you need.
    theFiles = dir(filePattern);
    s.patient_number = num;
    for k = 1 : length(theFiles)
        baseFile = theFiles(k).name;
        fullFileName = fullfile(folder, baseFile);
        fprintf(1, 'Now reading %s\n', fullFileName);
        %     variable allocation
        if strfind(baseFile, '1_T1')
            s.study1_T1file = fullFileName;
        elseif strfind(baseFile, '1_T2')
            s.study1_T2file = fullFileName;
        elseif strfind(baseFile, '1_FLAIR')
            s.study1_FLAIRfile = fullFileName;
        elseif strfind(baseFile, '2_T1')
            s.study2_T1file = fullFileName;
        elseif strfind(baseFile, '2_T2')
            s.study2_T2file = fullFileName;
        elseif strfind(baseFile, '2_FLAIR')
            s.study2_FLAIRfile = fullFileName;
        end
    end

    %% load and normalize data

    fields = fieldnames(s);
    fields = setdiff(fields,{'patient_number'});%%skip patient number, also skip the brain mask and the ground truth mask

    zscored = struct;

    for N = 1:numel(fields)
        field = fields{N};
        value = getfield(s, field);
        nii = load_nii(value);
        im = nii.img;
        temp = im;
        brainmask = getfield(load_nii(strcat('../MSpatientdata/patient',num,'/patient',num,'_brainmask.nii.gz')),'img')>0;
        centered = temp - mean(temp(brainmask));
        stddev = std(double(temp(brainmask)));
        zscored.(strrep(field,'file','')) = centered./stddev;
    end

    %% compute differences
    for N=1:3
        zscored.delta_T1 = getfield(zscored, 'study2_T1') - getfield(zscored, 'study1_T1');
        zscored.delta_T2 = getfield(zscored, 'study2_T2') - getfield(zscored, 'study1_T2');
        zscored.delta_FLAIR = getfield(zscored, 'study2_FLAIR') - getfield(zscored, 'study1_FLAIR');
    end

    %% save predictions as .nii
    fields = fieldnames(zscored);
    directoryName = strcat('../MSpatientdata-normalized/zscore/patient', num);
    if ~exist(directoryName, 'dir')
        mkdir(directoryName)
    end
    for N = 1:numel(fields)
        field = fields{N};
        z_vol = getfield(zscored, field);
        temp = make_nii(z_vol);   
        dummy_nii = load_nii(strcat('../MSpatientdata/patient',num,'/patient',num,'_study1_T2Wreg.nii.gz'));
        temp.hdr = dummy_nii.hdr;
        save_nii(temp, strcat(directoryName, '/patient', num, '_', field, '.nii.gz'));
        fprintf(1, 'Now created %s\n',field);

    end
end

        