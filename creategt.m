% generates ground truth with positive change labeled as 1, negatives labeled as 2
%patient_number: which patient to generate gt2 for
% visualize: whether to plot.
function [gt2, mask] = creategt(patient_number, visualize, save)
num = num2str(patient_number);
folder = strcat('../MSpatientdata/patient', num);
% Get a list of all files in the folder with the desired file name pattern
filePattern = fullfile(folder, '*.nii.gz'); % Change to whatever pattern you need.
theFiles = dir(filePattern);
% data = ones(2, 3);
s.patient_number = num;

for k = 1 : length(theFiles)
    baseFile = theFiles(k) .name;
    fullFileName = fullfile(folder, baseFile);
    fprintf(1, 'Now reading %s\n', fullFileName);
    %     variable allocation
    if strfind(baseFile, '1_T2')
        s.t2_s1file = fullFileName;
    elseif strfind(baseFile, '1_FLAIR')
        s.flair_s1file = fullFileName;
    elseif strfind(baseFile, '2_T2')
        s.t2_s2file = fullFileName;
    elseif strfind(baseFile, '2_FLAIR')
        s.flair_s2file = fullFileName;
    elseif strfind(baseFile, 'gt')
        s.gtfile = fullFileName;
    elseif strfind(baseFile, 'mask')
        s.maskfile = fullFileName;
    end
end

%% Generate intensities

nii = load_nii(s.maskfile);
mask = nii.img;
mask = logical(mask);
nii = load_nii(s.gtfile);
gt = nii.img;
gt2 = zeros(size(gt));
fields = fieldnames(s);

for N = 2:numel(fields) %skip patient number, go directly to index 2.
    field = fields{N};
    value = getfield(s, field);
    nii = load_nii(value);
    im = nii.img;
    r= strrep(field,'file','');
    temp = im(mask);
    %         centered = (temp - mean(temp)) ;
    %         sum = 0;
    %         for i = 1:numel(centered)
    %             sum = sum + (centered(i, 1)^2);
    %         end
    %         stddev = sqrt(double(sum)/length(centered));
    %     mean(temp)
    %     ints.(strcat(r,'ints')) = centered; %0 centered, now all stored in ints - should this be before or after difference calculation?
    ints3d.(strcat(r,'ints')) = zeros(size(mask));
    ints3d.(strcat(r,'ints'))(mask) = temp;
end
%% find connected regions
conn= bwconncomp(gt);
conn.labels = zeros(1, length(conn.PixelIdxList));
%% distances + blur
% dists.flair = smooth3(ints3d.flair_s2ints - ints3d.flair_s1ints,
% 'gaussian');'
dists.flair = ints3d.flair_s2ints - ints3d.flair_s1ints;
dists.t2 = smooth3(ints3d.t2_s2ints - ints3d.t2_s1ints, 'gaussian');
%% label each region in conn.labels

for N = 1:length(conn.PixelIdxList)
    % for N = 1:1
    temp = conn.PixelIdxList{N};
    [x, y, z] = ind2sub(size(dists.flair),temp);
    change = zeros(size(temp));
    for i = 1:length(x)
        change(i) =  (dists.flair(x(i),y(i),z(i)) > 0);
        %                 change(i) = (dists.flair(x(i), y(i), z(i)) > 0) | (dists.t2(x(i),y(i),z(i)) > 0);
        
    end
%     nnz(change)
%     length(temp)
    if nnz(change) > (0.25 * (length(temp)))
        conn.labels(N) = 1;
        %         %     if (nnz(change) == (length(temp)))
        %         conn.labels(N) = 'positive';
        %     else
        %         conn.labels(N) ='negative';
        %     end
        conn.labels
    end
end
%% visualize with text labels
islice = 52;
% subplot(1,2,1);
if visualize
    imagesc(gt(:,:,islice), [0, 1]);
    axis image;
    for N = 1:length(conn.labels)
        txt1 = '';
        if conn.labels(N) == 1
            txt1 = 'positive';
        else
            txt1 = 'negative';
        end
        temp = conn.PixelIdxList{N}(1);
        [x, y, z] = ind2sub(size(dists.flair),temp);
        if z == islice
            
            text(x, y, z,txt1);
        end
    end
end
%% visualize with different colors within connected componentss
% gt2(dists.flair > 0) = 1;
% gt2(dists.flair < 0) = 2;

for N = 1:length(conn.labels)
    
    temp = conn.PixelIdxList{N};
    [x, y, z] = ind2sub(size(dists.flair),temp);
    change = zeros(size(temp));
    for i = 1:length(x)
        change(i) =  (dists.flair(x(i),y(i),z(i)) > 0);
        %                 change(i) = (dists.flair(x(i), y(i), z(i)) > 0) | (dists.t2(x(i),y(i),z(i)) > 0);
        if conn.labels(N) == 0 %negative change
            gt2(x(i),y(i),z(i)) = 2;
        else
            gt2(x(i),y(i),z(i)) = 1;
        end
    end
    
    %     if conn.labels(N) == 1
    %         gt2(x, y, z) = 2;
    %     else
    %        gt2(x, y, z) = 1;
    %     end
    %
    
end
%%
if visualize
    figure;
    islice = 52;
    % subplot(1,2,1);
    imagesc(gt2(:,:,islice), [0, 2]);
    axis image;
end
%% save
if save
    temp = make_nii(gt2);    
    temp.hdr = getfield(load_nii(s.gtfile),'hdr');
    temp.hdr.dime.bitpix = 256; % signed char
    save_nii(temp, strcat('../MSpatientdata/patient', num, '/patient', num, '_gt3.nii'));

end