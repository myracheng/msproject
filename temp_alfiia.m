%%
addpath(genpath('./deps'));
%%
pth = '/MSpatientdata-normalized/zscore/';
%%
patients = [1,2,4,5,6,7,8,10,12,14,15,16,17,18,19]; %no AUC = 0 no disease progression from combine.m

%% load data from nii into allData and allGroundTruth
% patients =[1];
normmethod = 'zscore';
allDataTrain{J} = {};
allLabelsTrain{J} = {};
allMaskTrain{J} = {};
allDataTest{J} = {};
allLabelsTest{J} = {};
allMaskTest{J} = {};
for J = patients
    %% 
%     patient_number = patients(J); %change the particular patient's number
    num = num2str(J);
    %% load the images
    folder = strcat('../MSpatientdata/patient', num);
    % Get a list of all files in the folder with the desired file name pattern
    filePattern = fullfile(folder, '*.nii*'); % Change to whatever pattern you need.
    theFiles = dir(filePattern);
    s.patient_number = num;

    for k = 1 : length(theFiles)
        baseFile = theFiles(k).name;
        fullFileName = fullfile(folder, baseFile);
        fprintf(1, 'Now reading %s\n', fullFileName);
        %     variable allocation
        if strfind(baseFile, '1_T1')
            s.t1_s1file = fullFileName;
        elseif strfind(baseFile, '1_T2')
            s.t2_s1file = fullFileName;
        elseif strfind(baseFile, '1_FLAIR')
            s.flair_s1file = fullFileName;
        elseif strfind(baseFile, '2_T1')
            s.t1_s2file = fullFileName;
        elseif strfind(baseFile, '2_T2')
            s.t2_s2file = fullFileName;
        elseif strfind(baseFile, '2_FLAIR')
            s.flair_s2file = fullFileName;
        elseif strfind(baseFile, 'gt3')
            s.gtfile = fullFileName;
        elseif strfind(baseFile, 'mask')
            s.maskfile = fullFileName;
        end
    end
    %% load and normalize data

    nii = load_nii(s.maskfile);
    mask = nii.img;
    mask = logical(mask);
    nii = load_nii(s.gtfile);
    % load the new gt:
    gt = getfield(load_nii([folder,'/patient', num,'_gt3.nii']),'img');
    fields = fieldnames(s);
    fields = setdiff(fields,{'maskfile','gtfile','patient_number'});%%skip patient number, also skip the brain mask and the ground truth mask
    ints3d = struct;
    if strcmp(normmethod,'zscore')
        for N = 1:numel(fields)
            field = fields{N};
            value = getfield(s, field);
            nii = load_nii(value);
            im = nii.img;
            r = strrep(field,'file','');
            temp = im(mask);
            centered = (temp - mean(temp)) ;
            %         sum = 0;
            %         for i = 1:numel(centered)
            %             sum = sum + (centered(i, 1)^2);
            %         end
            stddev = std(double(temp));
            %     mean(temp)
            %     ints.(strcat(r,'ints')) = centered; %0 centered, now all stored in ints - should this be before or after difference calculation?
            ints3d.(strcat(r,'ints')) = zeros(size(mask));
            ints3d.(strcat(r,'ints'))(mask) = centered/stddev;
        end
       % normalize the intensities wrt WM
    elseif strcmp(normmethod,'wm')
        [ints3d.t1_s1ints, ints3d.t2_s1ints, ints3d.flair_s1ints] = normalizeWM(getfield(load_nii(s.t1_s1file),'img'),...
            getfield(load_nii(s.t2_s1file),'img'),...
            getfield(load_nii(s.flair_s1file),'img'), mask);
    end % otherwise the raw data
   
    % calc distances
    dist.t1_dist = ints3d.t1_s2ints - ints3d.t1_s1ints;
    dist.flair_dist = ints3d.flair_s2ints - ints3d.flair_s1ints;
    dist.t2_dist = ints3d.t2_s2ints - ints3d.t2_s1ints;

    %% load textureprops
    directoryName = strcat('texturepatches3/patient', num);
    hinfo = hdf5info(strcat(directoryName, '/patient', num,'_unbalancedtextureProps.h5'))
    data = h5read(strcat(directoryName, '/patient', num,'_unbalancedtextureProps.h5'),'/dataset1');
    inds = h5read(strcat(directoryName, '/patient', num,'_unbalancedtextureProps.h5'),'/inds');
    labels = h5read(strcat(directoryName, '/patient', num,'_unbalancedtextureProps.h5'),'/labels');
    %% get the voxel selection mask
    vsmask = zeros(size(gt));
    vsmask(inds) = 1;

    vsmask = logical(vsmask);
    Se = nnz(vsmask>0 & gt==1)/nnz(gt==1);
    fprintf('\nSe: %.2f\n',Se)
    fprintf('\nMissed: %d out of %d\n',nnz(vsmask==0 & gt==1), nnz(gt==1))
    %% get intensities
    data_deltaflair = dist.flair_dist(vsmask);
    data_deltaflair = (data_deltaflair-repmat(median(data_deltaflair,1),size(data_deltaflair,1),1))...
        ./repmat(2*(quantile(data_deltaflair,[.75],1)-quantile(data_deltaflair,[.25],1)),size(data_deltaflair,1),1);
    %% normalize the features
    data_norm = data;
    data_norm(isnan(data_norm))=0;
    data_norm = (data_norm-repmat(median(data_norm,1),size(data_norm,1),1))...
        ./repmat(2*(quantile(data_norm,[.75],1)-quantile(data_norm,[.25],1)),size(data_norm,1),1);
    %% balance the training data
    vsmask_balanced = zeros(size(vsmask));
    vsmask_balanced(vsmask>0 & gt==1) = 1; % select all positive class observations
    vsmask_balanced(randsample(find(vsmask>0 & gt == 0),1*nnz(vsmask>0 & gt==1))) = 1; % randomly select the same number of other observations
    vsmask_balanced = logical(vsmask_balanced);

    tmp = zeros(size(vsmask));
    tmp(vsmask) = 1:nnz(vsmask);
    %
    inds_balanced = tmp(vsmask & vsmask_balanced);
    %% assemble the final feature vector
    % balanced training
    data_train = [data_deltaflair(inds_balanced),data_norm(inds_balanced,:)];
    %data_train = [data_deltaflair(inds_balanced)];
    labels_train = labels(inds_balanced,:);
    mask_train = vsmask_balanced; 
    % unbalanced testing
    data_test = [data_deltaflair, data_norm];
    %data_test = [data_deltaflair];
    labels_test = labels;
    mask_test = vsmask; 
    %% concatenate the to the training feature set
    allDataTrain{J} = data_train;
    allLabelsTrain{J} = labels_train;
    allMaskTrain{J} = mask_train;
    allDataTest{J} = data_test;
    allLabelsTest{J} = labels_test;
    allMaskTest{J} = mask_test;
end
%% TODO: save the training and testing files
%% training
%% K-fold split
folds = 3;
% indices = crossvalind('Kfold', patients, folds);
testAUCs = [];
trainingpredAll = []; % voxelwise predictions
testGroundTruthAll = [];
indices = reshape(repmat([1,2,3], [5,1]), [15 1]);
%
for K = 1:folds
    testPatients = patients(indices == K);
    test=ismember(patients,testPatients);
    trainingPatients = patients(~test);
    %% init vars to add patient-by-patient
    trainingData = [];
    trainingGroundTruth = [];
    testData = [];
    testGroundTruth = [];
    %% load data for training and test
    for I = 1:length(patients)
         if ismember(patients(I), trainingPatients)
            trainingData = [trainingData; allDataTrain{I}];
            trainingGroundTruth = [trainingGroundTruth; allLabelsTrain{I}];
         else
            testData = [testData; allDataTest{I}];
            testGroundTruth = [testGroundTruth; allLabelsTest{I}];
         end
    end
    %% elastic net
    % trainingData(:,3:4) = [];
    [B, FitInfo] = lassoglm(double(trainingData),double(trainingGroundTruth),'binomial','CV',5,'Alpha',0.5);%
    % lassoPlot(B,FitInfo,'PlotType','CV');
    %
    index = FitInfo.IndexMinDeviance;%IndexMinMSE;
    wfeat = B(:,index);
    figure
    bar(wfeat)
    nnz(wfeat)
    %% sanity check on training
    XTest = trainingData;
    XTest(isnan(XTest) |isinf(XTest))=0;
    indx = FitInfo.IndexMinDeviance;
    B0 = B(:,indx);
    nonzeros = sum(B0 ~= 0)
    cnst = FitInfo.Intercept(indx);
    B1 = [cnst;B0];
    tic()
    trainingpred = glmval(B1,XTest,'logit'); %weights stored in B1
    toc()
    [X,Y,T,AUCROC,OPTROCPT] = perfcurve(double(trainingGroundTruth), trainingpred, 1,'xCrit','FPR',...
        'TVals',min(trainingpred):range(trainingpred)/100:max(trainingpred));%testGroundTruth{J}
    fprintf('\nTraining AUC: %.2f\n',AUCROC)
    %% Testing voxelwise:
    XTest = testData;
    XTest(isnan(XTest) |isinf(XTest))=0;
    tic()
    testpred = glmval(weights,XTest,'logit'); %weights stored in B1
    toc()
    [X,Y,T,AUCROC,OPTROCPT] = perfcurve(double(testGroundTruth), testpred, 1,'xCrit','FPR',...
        'TVals', min(testpred):range(testpred)/100:max(testpred) );%testGroundTruth{J}
    fprintf('\nTest (voxel-level) AUC: %.2f\n',AUCROC)
    trainingpredAll = [trainingpredAll;testpred]; 
    testGroundTruthAll = [testGroundTruthAll;testGroundTruth]; 
    %% TESTING patient-wise:
    for J = testPatients%(1)
        XTest = allDataTest{find(patients==J)};
        XTest(isnan(XTest) |isinf(XTest))=0;
        tic()
        testpred = glmval(weights,XTest,'logit'); %weights stored in B1
        toc()
        %     testpred = (data * b);
        % %         testpred = (data * b(2)) + b(1);
        %     testpred = 1./(1+exp(-testpred));
        %                     fprintf('end multiplication');
        
        %% create the volume
        pred_vol = zeros(size(allMaskTest{J}));
        pred_vol(allMaskTest{J}) = testpred;
        %%
        [X,Y,T,AUCROC,OPTROCPT] = perfcurve(double(allLabelsTest{J}), testpred, 1,'xCrit','FPR',...
            'TVals',min(testpred):range(testpred)/100:max(testpred));%testGroundTruth{J}
        fprintf('\nTest AUC: %.2f\n',AUCROC)
        testAUCs(find(patients==J)) = AUCROC;
    end
end
%% Overall voxelwise performance:
[X,Y,T,AUCROC,OPTROCPT] = perfcurve(double(testGroundTruthAll), trainingpredAll, 1,'xCrit','FPR',...
    'TVals', min(trainingpredAll):range(trainingpredAll)/100:max(trainingpredAll) );%testGroundTruth{J}
%%
fprintf('\nTest (voxel-level) AUC: %.2f\n',AUCROC)
fprintf('\nTest (patient-level) AUC mean: %.2f(%.2f)\n',mean(testAUCs(patients)),std(testAUCs(patients)))
fprintf('\nTest (patient-level) AUC median: %.2f(%.2f)\n',median(testAUCs(patients)),mad(testAUCs(patients)))
%%
[p,h] = signrank(testAUCs_flair,testAUCs)
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% view the feature matrices
clf, 
vsmask_balanced = zeros(size(vsmask));
vsmask_balanced(vsmask>0 & gt==1) = 1; % select all positive class observations
vsmask_balanced(randsample(find(vsmask>0 & gt == 0),1*nnz(vsmask>0 & gt==1))) = 1; % randomly select the same number of other observations
vsmask_balanced = logical(vsmask_balanced);

tmp = zeros(size(vsmask));
tmp(vsmask) = 1:nnz(vsmask);
%
inds_balanced = tmp(vsmask & vsmask_balanced);
data_norm = data;
data_norm(isnan(data_norm))=0;
% data_norm = (data_norm-repmat(min(data_norm,[],1),size(data_norm,1),1))...
%     ./repmat(max(data_norm,[],1)-min(data_norm,[],1),size(data_norm,1),1);
data_norm = (data_norm-repmat(median(data_norm,1),size(data_norm,1),1))...
    ./repmat(2*(quantile(data_norm,[.75],1)-quantile(data_norm,[.25],1)),size(data_norm,1),1);
% data_norm = (data_norm-repmat(mean(data_norm,1),size(data_norm,1),1))...
%     ./repmat(std(data_norm,[],1),size(data_norm,1),1);
[~, isort] = sort(labels(inds_balanced));
inds_sort = inds_balanced(isort);
subplot(1,2,1), imagesc(data_norm(inds_sort,:),[-1,1])%
subplot(1,2,2), imagesc(labels(inds_sort))
%% view the joint feature matrix
clf,
[~, inds_sort] = sort(labels_train);
subplot(1,2,1), imagesc(data_train(inds_sort,:),[-1,1])%
subplot(1,2,2), imagesc(labels_train(inds_sort))





