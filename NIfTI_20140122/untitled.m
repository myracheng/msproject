% Specify the folder where the files live.
myFolder = '/Users/myracheng/MSatientdata/patient1';
bview = false;
% Get a list of all files in the folder with the desired file name pattern.
filePattern = fullfile(myFolder, '*.nii.gz'); % Change to whatever pattern you need.
theFiles = dir(filePattern);
for k = 1 : length(theFiles)
  baseFileName = theFiles(k).name;
  fullFileName = fullfile(myFolder, baseFileName);
  fprintf(1, 'Now reading %s\n', fullFileName);
  % Now do whatever you want with this file name,
  % such as reading it in as an image array with imread()
  nii = load_nii(fullFileName);
  if bview
    view_nii(nii);
    drawnow; % Force display to update immediately.
  end
end