%% Training
trainingData = [];
groundTruth = [];
tic
for N = 1:20
    [d, g] = loaddata(N);
    trainingData = [trainingData; d];
    groundTruth = [groundTruth; g];

end
size(trainingData)
size(groundTruth)
%% LRM with Given Parameters
% initial_theta =zeros(10, 1);
% gt(gt > 50) = 1;
% gt(gt < 0.05) = 0;
% gt(gt >= 0.05) = 1;
% gtCat = categorical(gt);
% d = fitglm(double(trainingData), double(groundTruth))
[b, dev, stats] = glmfit(double(trainingData),double(groundTruth),'binomial', 'link', 'logit')
% c = mnrfit(double(data), doub
toc

%% validation
% gt2(gt2<0) = 0;
% validate(, gt2, 10);
