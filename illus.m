%% visualize methods

num = num2str(1);
flair = getfield(load_nii(strcat('../MSpatientdata/patient',num,'/patient',num,'_study1_FLAIRreg.nii.gz')),'img');
flair_2 = getfield(load_nii(strcat('../MSpatientdata/patient',num,'/patient',num,'_study2_FLAIRreg.nii.gz')),'img');
islice = 26;
gt = getfield(load_nii(strcat('../MSpatientdata/patient',num,'/patient',num,'_gt3.nii')),'img')==1;
gt = gt(:,:,islice)
colormap gray;
subplot(1,3,1);
imagesc(flipdim((flair_2(:,:,islice)-flair(:,:,islice))', 1));
set(gca,'xtick',[])
set(gca,'ytick',[])
f_dist = flair_2(:,:,islice)-flair(:,:,islice);
f_dist = f_dist - 100;
temp = f_dist;
f_dist(gt) = 200;
subplot(1,3,2);
imagesc(flipdim(f_dist', 1));
set(gca,'xtick',[])
set(gca,'ytick',[])
subplot(1,3,3);
mask = zeros(size(gt3));
directoryName = strcat('texturepatches3/patient', num);
inds = h5read(strcat(directoryName, '/patient', num,'_unbalancedtextureProps.h5'),'/inds');
mask(inds)= 1;
mask = logical(mask);
mask = mask(:,:,islice);
temp(mask) = 200;
imagesc(flipdim(temp', 1));
set(gca,'xtick',[])
set(gca,'ytick',[])
%% visualize results
imgs = {'baselinet', 'baselineneigbdfltext'};
for i = 1:length(imgs)
name = imgs{i};
J = 1
num = num2str(J);
folder = strcat('../genMSdata2/patient', num);
filePattern = fullfile(folder, strcat('*', name,'*')); % Change to whatever pattern you need.
theFiles = dir(filePattern);
s.patient_number = num;
fullFileName = [];
for k = 1 : length(theFiles)
baseFile = theFiles(k).name;
if strfind(name, 'text')
    proposed = getfield(load_nii(fullfile(folder, baseFile)),'img');
else
    baseline = getfield(load_nii(fullfile(folder, baseFile)),'img');
end
end
end
gt3 = getfield(load_nii(strcat('../MSpatientdata/patient',num,'/patient',num,'_gt3.nii')),'img')==1;
brain = getfield(load_nii(strcat('../MSpatientdata/patient',num,'/patient',num,'_study2_FLAIRreg.nii.gz')),'img');
temp = brain;
mask = zeros(size(gt3));
directoryName = strcat('texturepatches3/patient', num);
inds = h5read(strcat(directoryName, '/patient', num,'_unbalancedtextureProps.h5'),'/inds');
mask(inds)= 1;
mask = logical(mask);
islice = 26; %1_26
subplot(2,2,1)
imagesc(flipdim(gt3(:,:,islice)',1))
set(gca,'xtick',[])
set(gca,'ytick',[])
subplot(2,2,2) %prediction
imagesc(flipdim(proposed(:,:,islice)',1))
% brain = brain - std(brain(:)) / mean(brain(:));
denominator = max(brain) - min(brain);
    sub = min(brain);
    denominator = repmat(denominator, [size(brain,1),1]);
    sub = repmat(sub, [size(brain,1),1]);
        brain = (brain - sub) ./denominator;

binary = proposed > 0.8;
brain = brain/1.5;
set(gca,'xtick',[])
set(gca,'ytick',[])
brain(1,1,islice) = 1;
subplot(2,2,3) %prediction w/ FPR #2
denominator = max(temp) - min(temp);
    sub = min(temp);
    denominator = repmat(denominator, [size(temp,1),1]);
    sub = repmat(sub, [size(temp,1),1]);
        temp = (temp - sub) ./denominator;
binary = proposed > 0.75;
temp = temp/1.5;
temp(binary) = 1;
imagesc(flipdim(temp(:,:,islice)', 1))
% binary = proposed > 0.6;
% imagesc(brain(:,:,islice)) 
% set(h, 'AlphaData', alpha_data);
set(gca,'xtick',[])
set(gca,'ytick',[])

subplot(2,2,4) %prediction w/ baseline
binary = baseline>0.65;
% brain(1,1,islice) = 1;
brain(binary) = 1;
imagesc(flipdim(brain(:,:,islice)',1)) 
% imagesc(binary(:,:,islice)) 
set(gca,'xtick',[])
set(gca,'ytick',[])

%% colorful textures
pos = randsample(find(gt(inds) == 1), 200);
neg = randsample(find(gt(inds) == 0), 200);
gTtextureProps = data(pos,:);
textureProps = data(neg,:);
gTtextureProps;
textureProps;
concat = [gTtextureProps; textureProps];
% concat = concat';
denominator = max(concat) - min(concat);
denominator = repmat(denominator, [400,1]);
sub = min(concat);
sub = repmat(sub, [400,1]);
result = (concat - sub)./denominator;
gTtext = result(1:200,:);
text = result(201:400,:);
figure;

subplot(1,2,1);

imagesc(gTtext');
set(gca,'FontSize',18,'FontName','Times New Roman')
 

List = {'Mean',
'Standard Deviation',
'Skewness',
'Kurtosis',
'Autocorrelation',
'Contrast',
'Correlation',
'Cluster Prominence',
'Cluster Shade',
'Dissimilarity',
'Energy',
'Entropy',
'Homogeneity',
'Variance',
'Sum Average',
'Maximum Probability',
'SumVariance',
'SumEntropy',
'Difference Variance',
'Difference Entropy',
'Short RE',
'Long RE',
'Gray Level Non-Uniformity',
'Run Length Non-Uniformity',
'Run Percentage',
'Low Gray Level RE',
'High Gray Level RE',
'Short Run Low GE',
'Short Run High GE',
'Long Run Low GE',
'Long Run High GE'}

set(gca,'YTick',1:31,'YTickLabel',List, 'FontSize',14)
title('Sample Feature Values of Voxels with Lesion Change')

subplot(1,2,2);
xlabel('Voxel')
imagesc(text');
set(gca,'FontSize',14,'FontName','Times New Roman')
title('concat')
title('Sample Feature Values of Voxels with No Change')

set(gca,'YTick',1:31,'YTickLabel',' ', 'FontSize',14)
colorbar;


%% 1 sample wilcoxon test!
pval = [];
for i = 1:size(coef, 1)
p = signrank(coef(i,1:10));
pval = [pval, p];
end

%% find p values for AUC
p = zeros(4);
h = zeros(4);
for i = 1:size(AUC, 2)
    for j = 1:size(AUC, 2)
       [p(i, j), h(i,j)] = signrank(AUC(:,i), AUC(:,j));
       if h(i, j) == 1 && p(i, j) < 0.05 && p(i,j) > 0.01
           h(i, j) = 0.8;
       elseif i ~= j && h(i,j) == 1 && p(i,j) < 0.01;
           h(i,j) = 1;
       elseif i == j && h(i,j) == 0;
           h(i,j) = 0;
       else
           h(i,j) = 0.3;
       end
    end

end

%% AUC table
medians = median(AUC);
% [p, h] = signrank(allaucs, allaucs')
differences = repmat(medians,numel(medians),1) - repmat(medians', 1,numel(medians));
M = h;
% grayscale;
AuthorYearList = {'Method 1', 'Method 2', 'Method 3', 'Multi-Scale Method'};%
imagesc(M), axis square, box on
[x,y] = meshgrid(1:length(medians));   %# Create x and y coordinates for the strings
diftext = strcat('d = ', num2str(differences(:)), '1111');
medtext = strcat('p =', num2str(p(:)));
% test =sprintf(diftext,medtext)
% sprintf(diftext + medtext)
hStrings = text(x(:),y(:),strcat(num2str(differences(:),2),', ', num2str(p(:),2)),...      %# Plot the strings
                'HorizontalAlignment','center');
% hStrings = text(x(:),y(:),strcat(num2str(differences(:),2),num2str(p(:),2))...      %# Plot the strings
%                 'HorizontalAlignment','center');

            
set(gca,'xtick',1:size(medians,2))
set(gca,'ytick',1:size(medians,2))
colormap gray;
set(gca,'xticklabel',AuthorYearList)
% set(gca,'xticklabelrotation',45)
set(gca,'yticklabel',AuthorYearList)
%% visualize segmentations

imgs = {'baselineneigbdfltext'};
patients = [1]; %no AUC = 0 no disease progression from combine.m
% patients = [1,2,4,5,6,7,8,10,12,14,15,16,17,18,19]; %no AUC = 0 no disease progression from combine.m

%% calculate values for each
for i = 1:length(imgs)

    name = imgs{i};
    
    for J = patients %J is patient number
        %% load actual predictions
        num = num2str(J);
        folder = strcat('../genMSdata2/patient', num);
        filePattern = fullfile(folder, strcat('*', name,'*')); % Change to whatever pattern you need.
        theFiles = dir(filePattern);
        s.patient_number = num;
        fullFileName = [];
        for k = 1 : length(theFiles)
            baseFile = theFiles(k).name;
            if k == 1
                a = getfield(load_nii(fullfile(folder, baseFile)),'img');
            elseif k == 2
                b = getfield(load_nii(fullfile(folder, baseFile)),'img');
            elseif k == 3
                c = getfield(load_nii(fullfile(folder, baseFile)),'img');
            end
        end
        gt3 = getfield(load_nii(strcat('../MSpatientdata/patient',num,'/patient',num,'_gt3.nii')),'img')==1;
        if strfind(name, 'text')
            mask = zeros(size(gt3));
            directoryName = strcat('texturepatches3/patient', num);
            inds = h5read(strcat(directoryName, '/patient', num,'_unbalancedtextureProps.h5'),'/inds');
            mask(inds)= 1;
            mask = logical(mask);
      else
%             fprintf('yay')
            mask = getfield(load_nii(strcat('../MSpatientdata/patient',num,'/patient',num,'_brainmask.nii.gz')),'img')>0;
      
        end
%         gt = gt3(mask);
%         a = a(mask);
%         b = b(mask);
%         c = c(mask);
gt = gt3;
    
    iter = {a, b, c};
 figure;
        for i = 1:length(iter)
            hold on
           new = iter{i};
           new(isnan(new) |isinf(new))=0;
          
            thr = 0.5;

        seg = new>thr;
        % % nnz(seg)
        % seg
        islice = 27;
        subplot(2,length(iter),i);
        imagesc(seg(:,:,islice), [0, 1]);
        axis image;
        subplot(2,length(iter),i+length(iter));
        imagesc(gt(:,:,islice), [0, 1]);
        axis image;
        end
    end
end

% [p,h] = signrank(testAUCs_baseline,testAUCs)

te
%% glcm texture heat map
% gTtextureProps;
% textureProps;
% concat = [gTtextureProps, textureProps];
% concat = concat';
% coef = coef';
% init firstorder, glcm, glrlm
% denominator = max(coef(:)) - min(coef(:));
% denominator = repmat(denominator, [32,47]);
% sub = min(coef(:));
% sub = repmat(sub, [32,47]);
% result = (coef - sub)./denominator;
% figure;
imagesc(glcm);
glcmList = {'Autocorrelation',
'Contrast',
'Correlation',
'ClusterProminence',
'ClusterShade',
'Dissimilarity',
'Energy',
'Entropy',
'Homogeneity',
'Variance',
'SumAverage',
'MaximumProbability',
'SumVariance',
'SumEntropy',
'DifferenceVariance',
'DifferenceEntropy'}
title('Features Extracted from Gray-Level Co-occurence matrix')
colorbar
set(gca,'ytick',1:size(glcm,2))
xlabel('Run')
set(gca,'yticklabel',glcmList)

%% first-order texture heat map
% gTtextureProps;
% textureProps;
% concat = [gTtextureProps, textureProps];
% concat = concat';
% coef = coef';
% init firstorder, glcm, glrlm
% denominator = max(coef(:)) - min(coef(:));
% denominator = repmat(denominator, [32,47]);
% sub = min(coef(:));
% sub = repmat(sub, [32,47]);
% result = (coef - sub)./denominator;
figure;
denominator = max(firstorder) - min(firstorder);
denominator = repmat(denominator, [4,1]);
sub = min(firstorder);
sub = repmat(sub, [4,1]);
result = (firstorder - sub)./denominator;
imagesc(result2);
glcmList = {'Mean', 'StDev', 'Skewness', 'Kurtosis'}
title('First-Order Texture Features')
colorbar
set(gca,'ytick',1:size(firstorder,2))
xlabel('Run')
set(gca,'yticklabel',glcmList)
%% glcm texture heat map
% gTtextureProps;
% textureProps;
% concat = [gTtextureProps, textureProps];
% concat = concat';
% coef = coef';
% init firstorder, glcm, glrlm
% denominator = max(coef(:)) - min(coef(:));
% denominator = repmat(denominator, [32,47]);
% sub = min(coef(:));
% sub = repmat(sub, [32,47]);
% result = (coef - sub)./denominator;
% figure;
imagesc(glrlm);
glcmList = {'SRE',
'LRE',
'GLN',
'RLN',
'RP',
'LGRE',
'HGRE',
'SGLGE',
'SRHGE',
'LRLGE',
'LRHGE'}
title('Features Extracted from Gray-Level Run-Length Matrix')
colorbar
set(gca,'ytick',1:size(glrlm,2))
xlabel('Run')
set(gca,'yticklabel',glcmList)



%% histograms
for i =1:size(coef,1)
subplot(4, 8, i);
hist(coef(i,:));
end

%% first order features
% gTtextureProps;
% textureProps;
% concat = [gTtextureProps, textureProps];
% concat = concat';
% coef = coef';
% init firstorder, glcm, glrlm
% denominator = max(coef(:)) - min(coef(:));
% denominator = repmat(denominator, [32,47]);
% sub = min(coef(:));
% sub = repmat(sub, [32,47]);
% result = (coef - sub)./denominator;
% figure;
imagesc(glcm);
glcmList = {'Autocorrelation',
'Contrast',
'Correlation',
'ClusterProminence',
'ClusterShade',
'Dissimilarity',
'Energy',
'Entropy',
'Homogeneity',
'Variance',
'SumAverage',
'MaximumProbability',
'SumVariance',
'SumEntropy',
'DifferenceVariance',
'DifferenceEntropy'}
title('First-order Features Extracted from Intensities')
colorbar
set(gca,'ytick',1:size(glcm,2))
xlabel('Run')
set(gca,'yticklabel',glcmList)

%% visualize brain images (6 volumes)
num = num2str(1);
flair = getfield(load_nii(strcat('../MSpatientdata/patient',num,'/patient',num,'_study1_FLAIRreg.nii.gz')),'img');
t1 = getfield(load_nii(strcat('../MSpatientdata/patient',num,'/patient',num,'_study1_T1Wreg.nii.gz')),'img');
t2 = getfield(load_nii(strcat('../MSpatientdata/patient',num,'/patient',num,'_study1_T2Wreg.nii.gz')),'img');

flair_2 = getfield(load_nii(strcat('../MSpatientdata/patient',num,'/patient',num,'_study2_FLAIRreg.nii.gz')),'img');
t1_2 = getfield(load_nii(strcat('../MSpatientdata/patient',num,'/patient',num,'_study2_T1Wreg.nii.gz')),'img');
t2_2 = getfield(load_nii(strcat('../MSpatientdata/patient',num,'/patient',num,'_study2_T2Wreg.nii.gz')),'img');

islice = 26;
colormap gray;
subplot(2,3,1);
imagesc(flipdim(t1(:,:,islice)', 1));
title('{\itT1} at Time Point 1', 'FontName', 'Times New Roman', 'FontSize', 24)
set(gca,'xtick',[])
set(gca,'ytick',[])

subplot(2,3,2);
imagesc(flipdim(t2(:,:,islice)', 1));
title('{\itT2} at Time Point 1', 'FontName', 'Times New Roman', 'FontSize', 24)
set(gca,'xtick',[])
set(gca,'ytick',[])

subplot(2,3,3);
imagesc(flipdim(flair(:,:,islice)', 1));
title('{\itFLAIR} at Time Point 1', 'FontName', 'Times New Roman', 'FontSize', 24)
set(gca,'xtick',[])
set(gca,'ytick',[])

subplot(2,3,4);
imagesc(flipdim(t1_2(:,:,islice)', 1));
title('{\itT1} at Time Point 2', 'FontName', 'Times New Roman', 'FontSize', 24)
set(gca,'xtick',[])
set(gca,'ytick',[])

subplot(2,3,5);
imagesc(flipdim(t2_2(:,:,islice)', 1));
title('{\itT2} at Time Point 2', 'FontName', 'Times New Roman', 'FontSize', 24)
set(gca,'xtick',[])
set(gca,'ytick',[])

subplot(2,3,6);
imagesc(flipdim(flair_2(:,:,islice)', 1));
title('{\itFLAIR} at Time Point 2', 'FontName', 'Times New Roman', 'FontSize', 24)
set(gca,'xtick',[])
set(gca,'ytick',[])



%% visualize dissimilarity maps

num = num2str(1);
flair = getfield(load_nii(strcat('../MSpatientdata/patient',num,'/patient',num,'_study1_FLAIRreg.nii.gz')),'img');
t1 = getfield(load_nii(strcat('../MSpatientdata/patient',num,'/patient',num,'_study1_T1Wreg.nii.gz')),'img');
t2 = getfield(load_nii(strcat('../MSpatientdata/patient',num,'/patient',num,'_study1_T2Wreg.nii.gz')),'img');
flair_2 = getfield(load_nii(strcat('../MSpatientdata/patient',num,'/patient',num,'_study2_FLAIRreg.nii.gz')),'img');
t1_2 = getfield(load_nii(strcat('../MSpatientdata/patient',num,'/patient',num,'_study2_T1Wreg.nii.gz')),'img');
t2_2 = getfield(load_nii(strcat('../MSpatientdata/patient',num,'/patient',num,'_study2_T2Wreg.nii.gz')),'img');
islice = 26;
gt = getfield(load_nii(strcat('../MSpatientdata/patient',num,'/patient',num,'_gt3.nii')),'img')==1;
gt = gt(:,:,islice)
colormap gray;
subplot(2,3,1);
imagesc(flipdim((t1_2(:,:,islice)-t1(:,:,islice))', 1));
title('{\it\DeltaT1}', 'FontName', 'Times New Roman', 'FontSize', 24)
set(gca,'xtick',[])
set(gca,'ytick',[])

subplot(2,3,2);
imagesc(flipdim((t2_2(:,:,islice)-t2(:,:,islice))', 1));
title('{\it\DeltaT1}', 'FontName', 'Times New Roman', 'FontSize', 24)
set(gca,'xtick',[])
set(gca,'ytick',[])

subplot(2,3,3);
imagesc(flipdim((flair_2(:,:,islice)-flair(:,:,islice))', 1));
title('{\it\DeltaFLAIR}', 'FontName', 'Times New Roman', 'FontSize', 24)
set(gca,'xtick',[])
set(gca,'ytick',[])

t1_dist = t1_2(:,:,islice)-t1(:,:,islice);
t1_dist = t1_dist - 1000;
t1_dist(gt) = 200;
t2_dist = t2_2(:,:,islice)-t2(:,:,islice);
t2_dist = t2_dist - 1000;
t2_dist(gt) = 200;
f_dist = flair_2(:,:,islice)-flair(:,:,islice);
f_dist = f_dist - 100;
f_dist(gt) = 200;
subplot(2,3,4);
imagesc(flipdim(t1_dist', 1));
set(gca,'xtick',[])
set(gca,'ytick',[])

subplot(2,3,5);
imagesc(flipdim(t2_dist', 1));
set(gca,'xtick',[])
set(gca,'ytick',[])

subplot(2,3,6);
imagesc(flipdim(f_dist', 1));
set(gca,'xtick',[])
set(gca,'ytick',[])

%% visualize gt with TRaMS
num = num2str(1);
flair = getfield(load_nii(strcat('../MSpatientdata/patient',num,'/patient',num,'_study1_FLAIRreg.nii.gz')),'img');
flair_2 = getfield(load_nii(strcat('../MSpatientdata/patient',num,'/patient',num,'_study2_FLAIRreg.nii.gz')),'img');
islice = 26;
gt = getfield(load_nii(strcat('../MSpatientdata/patient',num,'/patient',num,'_gt3.nii')),'img')==1;
gt = gt(:,:,islice);
pred = getfield(load_nii(strcat('../genMSdata2/patient',num,'/patient',num,'_baselineneigbdfltext_1.nii.gz')),'img')>0.8;
pred = pred(:,:,islice);
pred2 = getfield(load_nii(strcat('../genMSdata2/patient',num,'/patient',num,'_baselinet_1.nii.gz')),'img')>0.8;
pred2 = pred2(:,:,islice);
f_dist = flair_2(:,:,islice)-flair(:,:,islice);
f_dist = f_dist - 100;
f_dist(pred) = 200;
colormap gray;
subplot(2,2,1);
imagesc(flipdim(f_dist', 1));
title('Patient 1 Slice 26 Results');
clear f_dist;
f_dist = flair_2(:,:,islice)-flair(:,:,islice);
f_dist = f_dist - 100;
f_dist(pred2) = 200;
subplot(2,2,3);
imagesc(flipdim(f_dist', 1));
title('Patient 1 Slice 26 Ground Truth');


num = num2str(19);
flair = getfield(load_nii(strcat('../MSpatientdata/patient',num,'/patient',num,'_study1_FLAIRreg.nii.gz')),'img');
flair_2 = getfield(load_nii(strcat('../MSpatientdata/patient',num,'/patient',num,'_study2_FLAIRreg.nii.gz')),'img');
islice = 31;
gt = getfield(load_nii(strcat('../MSpatientdata/patient',num,'/patient',num,'_gt3.nii')),'img')==1;
gt = gt(:,:,islice);
pred = getfield(load_nii(strcat('../genMSdata2/patient',num,'/patient',num,'_baselinet_1.nii.gz')),'img')>0.98;
pred = pred(:,:,islice);
pred2 = getfield(load_nii(strcat('../genMSdata2/patient',num,'/patient',num,'_baselinet_1.nii.gz')),'img')>0.98;
pred2 = pred2(:,:,islice);

f_dist = flair_2(:,:,islice)-flair(:,:,islice);
f_dist = f_dist - 100;
f_dist(pred) = 200;
colormap gray;
subplot(2,2,2);
imagesc(flipdim(f_dist', 1));
title('Patient 19 Slice 31 Results');

clear f_dist;
f_dist = flair_2(:,:,islice)-flair(:,:,islice);
f_dist = f_dist - 100;
f_dist(pred2) = 200;
subplot(2,2,4);
imagesc(flipdim(f_dist', 1));
title('Patient 19 Slice 31 Ground Truth');

%% auc curves for all patients
patients = [1,2,4,5,6,7,8,10,12,14,15,16,17,18,19]; %no AUC = 0 no disease progression from combine.m
name = 'baselineneigbdfltext'
roc = [];
pr = [];
for J = patients
    num = num2str(J);
        folder = strcat('../genMSdata2/patient', num);
        filePattern = fullfile(folder, strcat('*', name,'*')); % Change to whatever pattern you need.
        theFiles = dir(filePattern);
        s.patient_number = num;
        fullFileName = [];
        for k = 1 : length(theFiles)
            baseFile = theFiles(k).name;
            if k == 1
                a = getfield(load_nii(fullfile(folder, baseFile)),'img');
           end
        end
        gt3 = getfield(load_nii(strcat('../MSpatientdata/patient',num,'/patient',num,'_gt3.nii')),'img')==1;
        if strfind(name, 'text')
            mask = zeros(size(gt3));
            directoryName = strcat('texturepatches3/patient', num);
            inds = h5read(strcat(directoryName, '/patient', num,'_unbalancedtextureProps.h5'),'/inds');
            mask(inds)= 1;
            mask = logical(mask);
      else
%             fprintf('yay')
            mask = getfield(load_nii(strcat('../MSpatientdata/patient',num,'/patient',num,'_brainmask.nii.gz')),'img')>0;
      
        end
        gt = gt3(mask);
        a = a(mask);
    if k == 1
                a = getfield(load_nii(fullfile(folder, baseFile)),'img');
    end
    new = a;
           new(isnan(new) |isinf(new))=0;
             %% AUC, OPT
            [X,Y,T,AUCROC,OPTROCPT] = perfcurve(gt, new, true,'xCrit','FPR','TVals',min(new):range(new)/100:max(new));%testGroundTruth{J}
        hold on;

            plot(X, Y);
end
title('AUC-ROC Curves for TRaMS Method')
xlabel('False Positive Rate (1-Specificity)')
ylabel('Sensitivity')


%% boxplot of texture stuff
boxplot(w);
List = {'Mean',
'Standard Deviation',
'Skewness',
'Kurtosis',
'Autocorrelation',
'Contrast',
'Correlation',
'Cluster Prominence',
'Cluster Shade',
'Dissimilarity',
'Energy',
'Entropy',
'Homogeneity',
'Variance',
'Sum Average',
'Maximum Probability',
'SumVariance',
'SumEntropy',
'Difference Variance',
'Difference Entropy',
'Short RE',
'Long RE',
'Gray Level Non-Uniformity',
'Run Length Non-Uniformity',
'Run Percentage',
'Low Gray Level RE',
'High Gray Level RE',
'Short Run Low GE',
'Short Run High GE',
'Long Run Low GE',
'Long Run High GE'}
set(gca,'XTick',1:31,'XTickLabel',' ', 'FontSize',1) 

% colorbar
% set(gca,'xtick',1:size(w,2))
% set(gca,'xticklabel',List)
% set(get(gca,'XLabel'),'Rotation',180);
hx = get(gca,'XLabel');  % Handle to xlabel 
set(gca,'FontSize',18,'FontName','Times New Roman')

set(hx,'Units','data'); 
pos = get(hx,'Position'); 
y = pos(2); 
X = 1:31
% Place the new labels 
for i = 1:size(List,1) 
  t(i) = text(X(i),y,List(i,:)); 
end 
set(t,'Rotation',45,'HorizontalAlignment','right') 
ylabel('Coefficient Values')
set(t,'FontSize',12,'FontName','Times New Roman')
title('Coefficient Values of Radiomic Texture-Based Features')

%% auc boxplots
subplot(1,2,1)
boxplot(AUCROC);
set(gca,'xtick',1:size(AUCROC,2))
set(gca,'xticklabel',{'Method 1','Method 2','Method 3', 'Multi-Scale Method'})
set(gca,'FontSize',18,'FontName','Times New Roman')
% xlabel('Method')

title('Patient-Level AUC-ROC')
ylabel('AUC-ROC Value')
subplot(1,2,2)
boxplot(AUCPR);
set(gca,'xtick',1:size(AUCROC,2))
set(gca,'FontSize',18,'FontName','Times New Roman')
% % xlabel('Method')
set(gca,'xticklabel',{'Method 1','Method 2','Method 3', 'Multi-Scale Method'})
title('Patient-Level AUC-PR')

ylabel('AUC-PR Value')

